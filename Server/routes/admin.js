const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const {Subsection} = require('../models/subsection');
const {Section} = require('../models/section');
const {validation} = require('../models/user');
const bycrypt = require('bcryptjs');
const {User} = require('../models/user');
const Grid = require('gridfs-stream');
var email 	= require('../node_modules/emailjs/email');
const asyncmiddleware = require('../middleware/async');

// connect mongoose with grid
let conn = mongoose.connection;
Grid.mongo = mongoose.mongo;
let gfs;

// once connected
conn.once("open", () => {
    
    gfs = Grid(conn.db);
    

    // getfile returns the file with given id
    function getfile(id,imgflag){
        return new Promise((resolve,reject)=>{

            gfs.files.findOne({_id: id},((err, files) => {

                //console.log(files,"wtffff",id)
                let data = [];
                
                let readstream = gfs.createReadStream({
                    filename: files.filename
                });
    
                readstream.on('data', (chunk) => {
                    data.push(chunk);
                });
    
                readstream.on('end', () => {
                    data = Buffer.concat(data);
                    // if its image return in base64
                    if (imgflag){
                        let img = 'data:image/png;base64,' + Buffer(data).toString('base64');
                        resolve(img);
                    }
                    resolve(data);
                    
                });
    
                readstream.on('error', (err) => {
                  // if theres an error, respond with a status of 500
                  // responds should be sent, otherwise the users will be kept waiting
                  // until Connection Time out
                    reject(err);
                    //console.log('An error occurred!', err);
                });
            }));

        });

    }

    // gets all files in a subsection
    function getallfiles(subsection){
        return new Promise(async (resolve,reject)=>{
            const images = []
            const docs = []
            const subname = []
            for(let i =0; i < subsection.length;i++){

                subname.push(subsection[i].name);
            
                const img = await getfile(subsection[i].image,true);
                if(!img) reject(new Error({message : "error getting file"}));
                images.push(img);
                
                const doc = await getfile(subsection[i].document,false);
                if(!doc) reject(new Error({message : "error getting file"}));
                
                docs.push(doc);
                //console.log(docs.length,"thia ia doc")

            }
            /*await subsection.forEach(async (element)=>{
                subname.push(element.name);
            
                const img = await getfile(element.image,true);
                if(!img) reject(new Error({message : "error getting file"}));
                images.push(img);
                
                const doc = await getfile(element.document,false);
                if(!doc) reject(new Error({message : "error getting file"}));
                
                docs.push(doc);
                console.log(docs.length,"thia ia doc")
              
            });*/
            resolve([images,docs,subname]);
        });
    }

    // @route /api/admin/section/:sectionname get a particular section 
    router.get('/section', async(req, res) => {
        //let secname = req.params.sectionname;

        const section = await Section.find();
        if(!section) return res.status(404).send("Section not found");

        const data = []
        for(let i = 0 ; i < section.length;i++ ){
            const arr = await getallfiles(section[i].subsection);
            //console.log(arr[1].length,"this is arr")
            const subsection =[]
            for(let j=0 ; j < arr[0].length; j++){
                const obj = {
                    subname : arr[2][j],
                    image : arr[0][j],
                    doc : arr[1][j]
                }
                subsection.push(obj);
            }
            
            const resp = {
                name : section[i].name,
                subsection : subsection,
                description : section[i].description
            }

            data.push(resp);
        
        
        };


        
      //console.log(data.length);
      res.send(data);
    });

    function addfile(part){
        return new Promise((resolve,reject)=>{
            let result = []
            let i = 0;
             part.forEach(element => {
                let writeStream = gfs.createWriteStream({
                    filename: 'img_' + element.name,
                    mode: 'w',
                    content_type: element.mimetype
                });
        
                writeStream.on('close', (file) => {
                  // checking for file
                  i+=1
                  if(!file) {
                    reject('No file received');
                  }
                    result.push({
                        message: 'Success',
                        file: file
                    });
                    if(i == part.length){
                        resolve (result);
                    }
                    //console.log(result,i)
                });
                // using callbacks is important !
                // writeStream should end the operation once all data is written to the DB 
                writeStream.write(element.data, () => {
                  writeStream.end();
                });  
                
            });
        });
        
       
    }


    router.post('/addsection',async(req, res) => {
            
        function replaceall(exp,from,to){
            return exp.split(from).join(to).trim();
        }
        // adding images
        //console.log(req.body,req.files);
        //let part = req.files.images;
        const images = await addfile(req.files.images);
        const documents = await addfile(req.files.documents);
        const name = JSON.parse(req.body.utitle);
        const subsections = [];

        for( let i = 0; i < images.length ; i++){
            const subsection = new Subsection({
                name : name[i],
                image : images[i].file._id,
                document : documents[i].file._id
            });

            //const sub = await subsection.save();
            subsections.push(subsection);
            //console.log(subsection,'subbbbbbbbbbbbb');
        }
        
        const section = new Section({
            name : replaceall(req.body.usection,'"',''),
            subsection : subsections,
            description : replaceall(req.body.udescription,'"','')
        })

        const sec = await section.save();
        //console.log(sec)
        //console.log(req.files.images.length,req.files.documents.length)
        return res.status(200).send(sec);
  
        })
    ; 
});


    router.post('/deletesection/:name',async(req,res)=>{

        const section = await Section.findOneAndRemove({ name : req.params.name.replace('_',' ')});
        if(!section) return res.status(404).send("No section of req name found");
        //console.log(section.subsection);
        for(let i = 0; i < section.subsection.length; i++){
            
             //let file = await gfs.files.findOne({name : section.subsection[i].image })
             //gfs.remove({files_id : file._id})
             await gfs.remove({ _id: section.subsection[i].image},(err)=>{
                if(err) return res.status(500).send("error while deleting files")
                //console.log("deleted image")
            });

            //file = await gfs.files.findOne({name : section.subsection[i].document })
            //gfs.remove({files_id : file._id})
            await gfs.remove({ _id: section.subsection[i].document},(err)=>{
                if(err) return res.status(500).send("error while deleting files")
                //console.log("deleted doc")
            });

        }

        res.status(200).send("Deleted sucessfully");

    })

    router.get('/fetchUsers',async (req,res)=>{

        const users = await User.find({ approved : false});

        return res.status(200).send(users);
    })

    router.post('/approve',async (req,res)=>{
        //console.log(req.body.approveUser)
        let user = await User.findOne({email : req.body.approveUser});
        if(!user) return res.status(404).send("Invalid user");
        //console.log(user);
        user = await User.findByIdAndUpdate(user._id,{ $set: {approved : true}},{new : true});
        //console.log(user);
        if(!user) return res.status(404).send("Could not update ");

        var server 	= email.server.connect({
            user:    "rasaseschool@gmail.com", 
            password:"Rasas@123", 
            host:    "smtp.gmail.com", 
            ssl:     true
            });
            server.send({
             text:    "Welcome to the community! Your registration has been successfully approved by RASAS Admin. You can now Login to access Lesson Modules.", 
             from:    "RASAS Admin <skaneri7.sk@gmail.com>", 
             to:      "<"+req.body.approveUser+">",
             subject: "RASAS Registration Approved"
          }, function(err, message) { 
              //console.log("Error.....",err || "Sent.....",message); 
            });

          
        return res.status(200).send(user);
    })

    router.post('/reject',async (req,res)=>{

        let user = await User.findOneAndDelete({email : req.body.rejectUser});
        if(!user) return res.status(404).send("Invalid user");

        return res.status(200).send("User deleted");
    });

    router.get('/authenticate',async(req,res) => {
        ////console.log(req.sessionStore.sessions,req.sessionID);
        //console.log(req.sessionID);

        if(req.session.passport != undefined ){
            //console.log("here",req.session.passport)
            //console.log(req.session.passport.user.isAdmin,req.sessionID);
        //if( req.session.passport.user.isAdmin == false) //console.log("false")
        //else //console.log("true");
            if ( (! req.isAuthenticated() ) ||   (req.session.passport.user.isAdmin == false) ){
                //console.log("yes")
                res.status(403).send(false);
            }
            else{
                ////console.log("wtf",req.isAuthenticated());
                res.send(true);
            }
        }
        else{
            res.status(403).send(false);
        }
        
    });

    router.post('/register', async (req, res) => {
        const { error } = validation(req.body);
        if (error) return res.status(400).send(error.details[0].message);
        let user = await User.findOne({ email: req.body.email });
        if (user) return res.status(400).send('User already registered...');
    
        user = new User({
            first_name: req.body.first_name,
            middle_name: req.body.middle_name,
            last_name: req.body.last_name,
            email: req.body.email,
            contact: req.body.contact,
            connection: req.body.connection,
            purpose: req.body.purpose,
            isAdmin : req.body.isAdmin
        });
    
        const salt = await bycrypt.genSalt(10);
        user.password = await bycrypt.hash(req.body.password,salt);
    
        await user.save();
        res.status(200).send(user);
    });

    router.post('/authenticate', async (req,res,next)=>{
        ////console.log(req.sessionID,req.session.user);
        //console.log(req);
        const user = await User.findOne({"email":req.body.email});
        if(!user) return res.status(404).send("User not found");
        //console.log(user.isAdmin,"here is")
        if(user.isAdmin){
            //console.log("inside")
            passport.authenticate('local-login', (err,user,d)=>{
                if(err) return next(err);
                ////console.log("user",user,err,d);
                if(!user) return res.status(401).send({ success : false, message : 'authentication failed' });
        
                req.login( user , loginerr => {
                    if(loginerr) return next(loginerr);
                    return res.status(200).send(user);
                });
            })(req,res,next);
            
        }
        else res.status(403).send("Accesss Denied");
        
        });
    
        router.get('/logout',async(req,res)=>{
    
            req.logout();
            /*req.session.destroy((err)=>{
                if(err){
                    //console.log(err);
                }
            });*/
            //console.log("log out");
            res.send("loged out");
        })


module.exports = router;
