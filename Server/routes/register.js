const Joi = require('joi');
const mongoose = require('mongoose');
const express = require('express');
const {User} = require('../models/user');
const {validation} = require('../models/user');
const bycrypt = require('bcryptjs');
const passport = require('passport');
const router = express.Router();

router.post('/register', async (req, res) => {
    const request = {
        first_name: req.body.first_name,
        middle_name: req.body.middle_name,
        last_name: req.body.last_name,
        email: req.body.email,
        contact: req.body.contact,
        connection: req.body.connection,
        purpose: req.body.purpose
    }
    const { error } = validation(request);
    ////console.log(req.body,error.details[0].message)
    if (error) return res.status(400).send(error.details[0].message);
    let user = await User.findOne({ email: req.body.email });
    if (user) return res.status(400).send('User already registered...');
    //console.log("in here");
    user = new User({
        first_name: req.body.first_name,
        middle_name: req.body.middle_name,
        last_name: req.body.last_name,
        email: req.body.email,
        contact: req.body.contact,
        connection: req.body.connection,
        purpose: req.body.purpose
    });

    const salt = await bycrypt.genSalt(10);
    user.password = await bycrypt.hash(req.body.password,salt);

    await user.save();

    res.status(200).send(user);
});

router.post('/authenticate', (req,res,next)=>{
    ////console.log(req);
    //console.log(req.sessionID);
    passport.authenticate('local-login', (err,user,d)=>{
        if(err) return next(err);
        ////console.log("user",user,err,d);
        if(!user) return res.status(401).send({ success : false, message : 'authentication failed' });
        if(!user.approved) return res.status(403).send("User isn't approved yet");
        req.login( user , loginerr => {
            if(loginerr) return next(loginerr);
            ////console.log(req.session)
            return res.status(200).send(user);
        });
    })(req,res,next);
    });


    router.get('/authenticate',async(req,res) => {


        //console.log(req.session.passport,req.sessionID,req.isAuthenticated());
        if ( ! req.isAuthenticated() ){
            res.status(403).send(false);
        }
        else{
            res.send(true);
        }
    });

    router.post('/forgot', async (req, res) => {
        let user = await User.findOne({ email: req.body.email });
        if (!user) return res.status(400).send('User does not exist');
        else{
            const salt = await bycrypt.genSalt(10);
            let UpdatedPassword = await bycrypt.hash(req.body.password, salt);
        
            await User.updateOne({email: req.body.email},{$set: {password:UpdatedPassword}}, async function(err, res) {
                if (err) throw err;
            });
    
        }
        
        res.send("Updated");
    });
    
    router.get('/logout',async(req,res)=>{
    
        req.logout();
        /*req.session.destroy((err)=>{
            if(err){
                //console.log(err);
            }
        });*/
        //console.log("log out");
        res.send("loged out");
    })

module.exports = router;
