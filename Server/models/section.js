const mongoose = require('mongoose');
const Joi = require('joi');
const {subsectionschema} = require('./subsection');

const Section = mongoose.model('Section',new mongoose.Schema({
    name : {
        type : String,
        required : true,
        maxlength : 256
    },
    subsection : {
        type : [subsectionschema],
        //value : [Subsection],
        required : true
    },
    description : {
        type : String,
        maxlength : 256
    }

}));

function validate(exp){
    const schema = {
        name : Joi.string().min(3).required(),
        subsection : Joi.required(),
        description : Joi.string().min(3).max(256).required()
    }

    return result = Joi.validate(exp,schema);

}

exports.Section = Section;
exports.validate = validate;
