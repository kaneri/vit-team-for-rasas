const mongoose = require('mongoose');
const Joi = require('joi');


const User = mongoose.model('User', new mongoose.Schema({
    first_name:{
        type: String,
        required: true
    },
    middle_name:{
        type: String
    },
    last_name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    isAdmin:{
        type : Boolean,
        required : true,
        default : true
    }
    
}));


function validation(Register) {
    const valid = {
        first_name: Joi.string().min(1),
        middle_name: Joi.string(),
        last_name:Joi.string().min(1),
        email: Joi.string().email(),
        password: Joi.string(),
        isAdmin : Joi.boolean()
    };
    return Joi.validate(Register, valid);
}

exports.Admin = Admin;
module.exports.validation = validation;