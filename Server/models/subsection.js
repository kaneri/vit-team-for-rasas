const mongoose = require('mongoose');
const Joi = require('joi');
const subsectionschema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        maxlength : 256
    },
    image : {
        type : mongoose.Schema.Types.ObjectId,
        required : true
    },
    document : {
        type : mongoose.Schema.Types.ObjectId,
        required : true
    }

})


const Subsection = mongoose.model('Subsection',subsectionschema);

function validate(exp){
    const schema = {
        name : Joi.string().min(3).required(),
        image : Joi.ObjectID().required(),
        document : Joi.ObjectID().required()
    }

    return result = Joi.validate(exp,schema);

}

exports.Subsection = Subsection;
exports.validate = validate;
exports.subsectionschema = subsectionschema;