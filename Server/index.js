const express = require('express');
const app = express();

const path = require("path");
app.use(express.static(path.join(__dirname, "../build")))


//app.use(error);
app.listen(3001,()=>{
    console.log("Listening on port ",3001);
});

