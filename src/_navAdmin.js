export default {
    items: [
      {},
      {
        name: 'Home',
        url: '/dashboard',
        icon: 'icon-home',
      },
      {
        name: 'Philosophy',
        url: '/philosophy',
        icon: 'icon-pencil',
      },
      {
        name: 'Process',
        url: '/process',
        icon: 'icon-speedometer',
      },
      {
        name: 'Assess',
        url: '/assess',
        icon: 'icon-calculator',
      },
      {
        name: 'Time-Table',
        url: '/timetable',
        icon: 'icon-puzzle',
      },
      {
        name: 'Admin Lesson Modules',
        url: '/Lesson_Modules_Admin',
        icon: 'icon-star',
      },
      {
        name: 'Upload Section',
        url: '/upload',
        icon: 'icon-plus',
      }, 
      {
        name: 'Manage Users',
        url: '/Approve',
        icon: 'icon-user',
      },
      {
        name: 'Login/Logout',
        url: '/Login',
        icon: 'icon-star',
      } 
    ],
  };
  