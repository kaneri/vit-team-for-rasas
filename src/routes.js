import React from 'react';
import Loadable from 'react-loadable'



function Loading() {
  return <div>Loading...</div>;
}

const Approve = Loadable({
  loader: () => import('./views/Admin/Approve'),
  loading: Loading,
});

const Upload = Loadable({
  loader: () => import('./views/Admin/Upload'),
  loading: Loading,
});


const LMA = Loadable({
  loader: () => import('./views/Admin/Lesson_modules'),
  loading: Loading,
});

const Login = Loadable({
  loader: () => import('./views/Pages/Login'),
  loading: Loading,
});

const Forgot = Loadable({
  loader: () => import('./views/Pages/Login/Forgot'),
  loading: Loading,
});

const Register = Loadable({
  loader: () => import('./views/Pages/Register'),
  loading: Loading,
});

const Assess = Loadable({
  loader: () => import('./views/Assess'),
  loading: Loading,
});


const Process = Loadable({
  loader: () => import('./views/Process'),
  loading: Loading,
});

const Timetable = Loadable({
  loader: () => import('./views/Timetable'),
  loading: Loading,
});

const Philosophy = Loadable({
  loader: () => import('./views/Philosophy/Philosophy'),
  loading: Loading,
});

const Dashboard = Loadable({
  loader: () => import('./views/Dashboard'),
  loading: Loading,
});

const Lesson_modules = Loadable({
  loader: () => import('./views/Lesson_modules'),
  loading: Loading,
});



// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: Dashboard },
  { path: '/process', name: 'Process', component: Process },
  { path: '/Timetable', name: 'Timetable', component: Timetable },
  { path: '/dashboard', name:'Dashboard', component: Dashboard},
  { path: '/Assess', name: 'Assess', component: Assess },
  { path: '/philosophy', exact: true,  name: 'Philosophy', component: Philosophy },
  { path: '/login', exact: true,  name: 'Login', component: Login },
  { path: '/forgot', exact: true,  name: 'Forgot', component: Forgot },
  { path: '/register', exact: true,  name: 'Register', component: Register },
  { path: '/Lesson_modules', name: 'Lesson_modules', component: Lesson_modules },
  { path: '/Lesson_modules_Admin', name: 'Lesson_modules', component: LMA },
  { path: '/upload', name: 'Upload Section', component: Upload },
  { path: '/approve', name: 'Approve Users', component: Approve }
];

export default routes;
