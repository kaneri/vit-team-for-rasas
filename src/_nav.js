export default {
  items: [
    {},
    {
      name: 'Home',
      url: '/dashboard',
      icon: 'icon-home',
    },
    {
      name: 'Philosophy',
      url: '/philosophy',
      icon: 'icon-pencil',
    },
    {
      name: 'Process',
      url: '/process',
      icon: 'icon-speedometer',
    },
    {
      name: 'Assess',
      url: '/assess',
      icon: 'icon-calculator',
    },
    {
      name: 'Time-Table',
      url: '/timetable',
      icon: 'icon-puzzle',
    },
    {
      name: 'Lesson Modules',
      url: '/Lesson_modules',
      icon: 'icon-star',
    },
    {
      name: 'Login/Logout',
      url: '/Login',
      icon: 'icon-star',
    }    
  ],
};
