import React, { Component } from 'react';
import {
  Row,
  Col,
  Card,
  CardImg
} from 'reactstrap'
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table'
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css'
import "./style1.css";
class Assess extends Component {
  render() {
    return (
      <div className="">
        <Row style={{ 'margin-bottom': '0px' }}>
          <Col xs="12" sm="12" lg="12" style={{ 'margin-bottom': '0px' }}>
            <div id="classroom-rules" class="text-center card-head" style={{ 'margin-bottom': '0px' }}>
              <h3><b>ASSESSMENT FRAMEWORK</b></h3>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <div id="assg-objective" class="cool-font">
              <h6><b>The primary objective of assessing is to understand education and learning is a lifelong process and its
                important to take small baby steps to keep moving in the healthy growth to continue and
                the speed or acceleration of the progress is a non-issue.
						<br /><br />
                In a healthy growing child - devotion to Krishna is nurtured, inspering devotional qualities manifest, gains
                authentic personal character, enriches the collective devotional culture in such a way that every exchange
                encountered is an opportunity to transmit knowledge, with consideration and compassion.
						<br /><br />
                To have a teacher who knows where the strengths id student are, who will go out of their way, to find ways
                to nuture that for them is truly blessed.
					</b></h6>
            </div>
          </Col>
        </Row>
        <Row style={{ 'margin-bottom': '0' }}>
          <Col xs="12" sm="12" lg="12">
            <Card className="border-0" id="trans">
              <CardImg src={require('./4 PART ASSESSMENT.jpg')} />
            </Card>
          </Col>
        </Row>
        <Row>
            <Col xs="12" sm="12" lg="12">
              <div id="heading" class="text-center card-head">
                <h1>Do You Want To Assess The Level Of Your Child? </h1><h6><a href={require("./assessment.pdf")} download={'Assessment.pdf'}><spam class="cool-font">Click here</spam></a></h6>
              </div>
            </Col>
          </Row>
        <div style={{'margin': '0 25px 0 25px'}}>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Table bordered id="t1">
              <col width="10%" style={{ 'background-color': '#ff8533' }} class="bordered"/>
              <col width="20%" class="bordered"/>
              <col width="28%" class="bordered"/>
              <col width="42%" class="bordered"/>

              <tbody class="card-head">
                <tr style={{ 'background-color': 'lightgray' }}>
                  <th id="head"></th>
                  <th id="head">ASPECTS TO ASSESS</th>
                  <th id="head">PARTICULARS TO BE ASSESSED</th>
                  <th id="head">LINKS FOR REFERENCE</th>
                </tr>
                <tr class="bordered">
                <td rowSpan="27" id="vert-align" class="text-white text-center">VYASHTI RELATIONSHIP WITH SELF</td>
                  <td rowSpan="13">SELF CARE</td>
                  <td>
                    <tr>HEALTH</tr>
                  </td>
                  <td class="cool-font">
                    <p><a href="https://drwalt.com/PDF/Assessteenhealth.pdf">Assessteenhealth.pdf</a><br />
                      <a href="https://drwalt.com/PDF/Assesschildhealth.pdf">Assesschildhealth.pdf</a><br />
                      <a href="https://www.healthline.com/health/childrens-health-symptoms#extreme-fatigue">childrens-health-symptoms#extreme-fatigue</a></p>
                  </td>
                </tr>
                <tr class="bordered">
                  <td rowSpan="5">DIET</td>
                  <td>CALORIE REQUIREMENT-TOTAL</td>
                </tr>
                <tr class="bordered">
                  <td>FRUIT, VEGETABLES, GRAINS, PROTEIN, DAIRY - INTAKE</td>
                </tr>
                <tr class="bordered">
                  <td>VITAMINS, MINERALS & TRACE ELEMENTS IN DIET</td>
                </tr>
                <tr class="bordered">
                  <td>WATER INTAKE</td>
                </tr>
                <tr class="bordered">
                  <td style={{ 'padding': '10px' }}></td>
                </tr>

                <tr class="bordered">
                  <td>SLEEP</td>
                  <td class="cool-font"><a href="https://my.clevelandclinic.org/health/articles/14306-healthy-sleep-habits-for-children">healthy-sleep-habits-for-children</a><br /><br />
                    <a href="https://www.healthychildren.org/English/healthy-living/sleep/Pages/Healthy-Sleep-Habits-How-Many-Hours-Does-Your-Child-Need.aspx">Sleep-Habits-How-Many-Hours-Does-Your-Child-Need.aspx</a><br /><br />
                    <a href="http://www.lssc.edu/faculty/leonardo_rodriguez/Downloads%20%20Documents/Physical%20Fitness%20Classes/Materials/Ch11_Fitness_Books.pdf">Fitness Book</a><br /><br />
                    <a href="http://facta.junis.ni.ac.rs/pe/pe201302/pe201302-02.pdf">assessment of physical fitness in children and adolescents</a></td>
                </tr>
                <tr class="bordered">
                  <td rowSpan="5">EXERCISE</td>
                </tr>
                <tr class="bordered">
                  <td>PHYSICAL EXERCISE</td>
                </tr>
                <tr class="bordered">
                  <td>MENTAL EXERCISE</td>
                </tr>
                <tr class="bordered">
                  <td>BREATHING EXERCISE</td>
                </tr>
                <tr>
                  <td style={{ 'padding': '10px' }}></td>
                </tr>

                <tr class="bordered">
                  <td style={{ 'padding-bottom': '30px' }}>LIFESTYLE(IN GOODNESS)</td>
                  <td class="cool-font"><a href="https://www.kidslifestudio.com/assessments/premium-assessments/">premium-assessments</a></td>
                </tr>

                <tr class="bordered">
                  <td rowSpan="8">SELF DEVELOPMENT</td>
                  <td>SELF RELIANT</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>LEARN NEEDED KSV(ABILITIES & INTERESTS)</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>VISIONING AND GOAL SETTING</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>GET MENTORED BY EXPERTS</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>SERVICE ATTITUDE & NON-ENVY</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>INTEGRITY & SELF CONTROL</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>MATURITY</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td style={{ 'padding': '10px' }}></td>
                  <td style={{ 'padding': '10px' }}></td>
                </tr>

                <tr class="bordered">
                  <td rowSpan="6">SADACHAR</td>
                  <td>TRUTHFULNESS</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>
                    CLEANLINESS
                    </td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>
                    COMPASSION / SENSITIVE BEHAVIOUR
                    </td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>
                    SHARING / COLLABORATING
                    </td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>
                    PUNCTUALITY / VALUING TIME
                    </td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td style={{ 'padding': '10px' }}></td>
                  <td></td>
                </tr>
              </tbody>

            </Table>
          </Col>
          <Col xs="12" sm="12" lg="12">
            <Table bordered id="t2" class="bordered">
              <col width="10%" style={{ 'background-color': '#6c3f99' }} />
              <col width="20%" class="bordered"/>
              <col width="28%" class="bordered"/>
              <col width="42%" class="bordered"/>

              <tbody class="card-head">
                <tr>
                  <td rowSpan="4" id="vert-align" class="text-center text-white">SAMASHTI RELATIONSHIP WITH PEOPLE</td>
                  <td rowSpan="4">RELATIONSHIPS</td>
                  <td class="padding-big">PERSONAL / PARENTS</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td class="padding-big">SCHOOL / FRIENDS</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td class="padding-big">EXTENDED / TEACHERS etc</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td class="padding-big">DEVOTEES</td>
                  <td></td>
                </tr>
              </tbody>
            </Table>
          </Col>

          <Col xs="12" sm="12" lg="12">
            <Table bordered id="t3">
              <col width="10%" style={{ 'background-color': '#6C8F00' }} />
              <col width="20%" class="bordered"/>
              <col width="28%" class="bordered"/>
              <col width="42%" class="bordered"/>

              <tbody class="card-head">
                <tr>
                  <td rowSpan="5" id="vert-align" class="text-center text-white">SRISHTI RELATIONSHIP WITH NATURE</td>
                  <td rowSpan="5">RELATIONSHIPS</td>
                  <td class="padding-big">STRATEGIC PLANNING</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td class="padding-big">EFFECTIVE COMMUNICATION & MARKETING</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td class="padding-big">RESPONSIBLE ENGAGEMENT OF RESOURCES</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td class="padding-big">ASSESSMENT & ACCOUNTABILITY</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td class="padding-big">EFFECTIVE STRUCTURES & SYSTEMS</td>
                  <td></td>
                </tr>
              </tbody>
            </Table>
          </Col>

          <Col xs="12" sm="12" lg="12">
            <Table bordered id="t4">
            <col width="10%" style={{ 'background-color': '#FBBEF4','color':'black !important' }} class="bordered"/>              <col width="20%" class="bordered"/>
              <col width="28%" class="bordered"/>
              <col width="42%" class="bordered"/>

              <tbody class="card-head">
                <tr>
                  <td rowSpan="15" id="vert-align" class="text-center text-white">PARAMESHTI RELATIONSHIP WITH GOD</td>
                  <td rowSpan="7">SHIKSHA</td>
                  <td>PHILOSOPHY</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>CULTURE</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>ALIGNMENT WITH SRILA PRABHUPADA / PRINCIPLES</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>MAINTAINING SPIRITUAL PRIORITIES</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>CREATIVE VISION</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>UNITY IN DIVERSITY</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td style={{ 'padding': '10px' }}></td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td rowSpan="4">
                    SADHANA
                    </td>
                  <td>JAPA / KIRTAN</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>DEITY WORSHIP</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>STUDY / HEARING OF SCRIPTURES</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td style={{ 'padding': '10px' }}></td>
                  <td style={{ 'padding': '10px' }}></td>
                </tr>
                <tr class="bordered">
                  <td rowSpan="4">DEVOTIONAL SKILLS</td>
                  <td>SINGING</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>PLAYING MUSICAL INSTRUMENTS</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td>TRADITIONAL SPIRITUAL DANCES</td>
                  <td></td>
                </tr>
                <tr class="bordered">
                  <td style={{ 'padding': '10px' }}></td>
                  <td style={{ 'padding': '10px' }}></td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
      </div>








    );
  }
}

export default Assess;
