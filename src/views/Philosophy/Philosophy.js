import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import {
  Card,
  CardBody,
  CardFooter,
  CardGroup,
  CardHeader,
  CardImg,
  CardImgOverlay,
  Col,
  Row
} from 'reactstrap';
import "./dashboard.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";


class Philosophy extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  render() {

    // const userList = usersData.filter((user) => user.id < 10)

    return (
      <div className="animated fadeIn" style={{ 'color': '#000000' }}>
        <Row>
          <Col>
            <Card>
              <CardBody>

                <Carousel autoPlay="true" infiniteLoop="true" showThumbs="false"  >
                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./slideshow-1.jpg')} />
                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./slideshow-2.jpg')} />
                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./slideshow-3.jpg')} />
                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./slideshow-4.jpg')} />
                  </div>
                </Carousel>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardHeader className="text-center card-head">
                <b><i>PHILOSOPHY OF EDUCATION</i></b>
              </CardHeader>
              <CardBody className="cool-font">
                <p style={{ 'font-size': '20px' }}>This website explores paradigms, principles and practices of holistic education. It is based on the teaching of our Founder Acharya, His Divine Grace A.C. BHAKTIVEDANTA SWAMI SRILA PRABHUPADA, his books like Sri Isopanishad, Nectar of instruction, Nector of devotion and Bhagvad-Gita As it is and other ancient and modern educational theories.
                </p>
              </CardBody>
              <CardFooter></CardFooter>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" lg="4">
            <Card>
              <CardBody>
                <div class="text-center card-head">
                  <h3><b>WHAT IS OUR EDUCATION AIM</b></h3>
                </div>
                <img class="card-img-top rounded" src={require('./WHAT IS OUR EDUCATION AIM.jpg')} alt="Card image cap" style={{ 'height': '250px' }} />
                <br /><br />
                <div class="text-center card-head">
                  <h3><b>WHAT IS THE LEARNING OUTCOME</b></h3>
                </div>
                <img class="card-img-top rounded" src={require('./WHAT IS THE LEARNING OUTCOME.jpg')} alt="Card image cap" style={{ 'height': '250px' }} />
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="12" lg="8">
            <Card>
              <CardHeader></CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="12" lg="6">
                    <h4><b>Self Development</b></h4>
                    <p class="cool-font">Enhance the physical, intellectual, emotional and moral wellness of children so as to transform them into refined, cultured and contributing gentlemen.</p>
                  </Col>
                  <Col xs="12" sm="12" lg="6">
                    <h4><b>Inter-personal Relationships</b></h4>
                    <p class="cool-font">To teach children how to relate with others in such a way that it brings happiness to oneself and others.</p>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="12" lg="12">
                   <img  class="card-img-top rounded" src={require('./objectives-cropped.jpg')} alt="Card image cap" style={{ 'height': '350px' }} /> 
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="12" lg="6">
                    <h4><b>Spiritual Development</b></h4>
                    <p class="cool-font">Teach children to respect all religions and develop universal brotherhood by instilling respect for all life forms as children of a loving God.</p>
                  </Col>
                  <Col xs="12" sm="12" lg="6">
                    <h4><b>Social responsibility</b></h4>
                    <p class="cool-font">To inculcate responsility towards environment and society by helping children grow into conscientious adults.</p>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter></CardFooter>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardHeader></CardHeader>
              {/* <CardBody> */}
              <CardGroup>
                {/* <Col xs="12" sm="12" lg="3"> */}
                <Card>
                  <CardImg class="card-img-top" src={require('./self development.jpg')} alt="Card image cap" style={{ 'height': '100%' }} />
                  <CardImgOverlay style={{ 'padding': '0' }}>
                  </CardImgOverlay>
                </Card>
                {/* </Col> */}
                {/* <Col xs="12" sm="12" lg="3"> */}
                <Card>
                  <CardImg class="card-img-top" src={require('./dev rel with other people.jpg')} alt="Card image cap" style={{ 'height': '100%' }} />
                  <CardImgOverlay style={{ 'padding': '0' }}>
                  </CardImgOverlay>
                </Card>
                {/* </Col> */}
                {/* <Col xs="12" sm="12" lg="3"> */}
                <Card>
                  <CardImg class="card-img-top" src={require('./dev rel with nature.jpg')} alt="Card image cap" style={{ 'height': '100%' }} />
                  <CardImgOverlay style={{ 'padding': '0' }}>
                  </CardImgOverlay>
                </Card>
                {/* </Col> */}
                {/* <Col xs="12" sm="12" lg="3"> */}
                <Card>
                  <CardImg class="card-img-top" src={require('./dev rel with God.jpg')} alt="Card image cap" style={{ 'height': '100%' }} />
                  <CardImgOverlay style={{ 'padding': '0' }}>
                  </CardImgOverlay>
                </Card>
              </CardGroup>
              {/* </Col> */}

              <div><br /><br /></div>
              <CardGroup>
                <Card style={{ 'background-color': 'azure' }}>

                  <div class="text-center card-head" style={{ 'height': '150px' }}>
                    <h2><b>RELATIONSHIP WITH </b></h2>
                    <h1><b>SELF</b></h1>
                    <br />
                  </div>

                </Card>
                <Card style={{ 'background-color': 'lightcyan' }}>

                  <div class="text-center card-head" style={{ 'height': '150px' }}>
                    <h2><b>RELATIONSHIP WITH </b></h2>
                    <h1><b>PEOPLE</b></h1>
                    <br />
                  </div>

                </Card>
                <Card style={{ 'background-color': 'rgb(191, 253, 253)' }}>

                  <div class="text-center card-head" style={{ 'height': '150px' }}>
                    <h2><b>RELATIONSHIP WITH </b></h2>
                    <h1><b>NATURE</b></h1>
                    <br />
                  </div>

                </Card>
                <Card style={{ 'background-color': 'rgb(125, 252, 252)' }}>

                  <div class="text-center card-head" style={{ 'height': '150px' }}>
                    <h2><b>RELATIONSHIP WITH </b></h2>
                    <h1><b>GOD</b></h1>
                    <br />
                  </div>

                </Card>
              </CardGroup>
              {/* </CardBody> */}
              <CardFooter></CardFooter>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardHeader className="card-head">
                <h3><b>WANT TO KNOW MORE....</b></h3>
              </CardHeader>
              <CardBody className="card-head">
                <h2>1. Self Development -Vyashti</h2>
                <p class="cool-font">
                  Education is for knowing oneself.Self-realization operates on three levels.<br />
                  <h4><b>I. Autonomy</b></h4>
                  <p>
                    Ya vidya sa vimuktaye = education must lead to salvation/ freedom from <br />
                    conditioning and one's lower nature. One should become self<br />
                    sufficient and independent. Truly independent persons can then become interdependent.
                </p>
                  <h4><b>II. Purpose</b></h4>
                  <p>
                    Through education, one develops a clear sense of meaning and purpose to one's life based on the following factors : <br />
                    Perspective - darshana - one's philosophy of life<br />
                    Desire - proper use of free will - proactivity - proper sankalpa
                </p>
                  <h4><b>III. Mastery</b></h4>
                  <p>
                    Developing one's abilities and interests - <br />
                    Developing one's knowledge -<br />
                    Proper control over one's thoughts words and action - performing<br />
                    prescribed duties given in scriptures in dutiful, detached and devotional mood<br />
                    Flow experience - be able to absorb oneself in tasks at hand with individual consciousness connected to the universal consciousness - samadhi.
                </p>
                </p>
                <h2>2. Developing Relationship with other peope - Samashti</h2>
                <p class="cool-font">
                  Ability to build trust and extend trust, to make and keep commitments being exemplary<br />
                  Ability to build common vision/ purpose while working with others <br />
                  Ability to understand others and deal with them with *atmiyata[Ref - BG6.32]<br />
                  Ability to care for others and communicate effectively<br />
                  To offer unconditional service, being non-envious<br />

                  Root cause of breakdown in relationship is disobedience to the instruction given by Lord<br />
                  By reinstating oneself in practice of pure devotional service one can heal and rebuild even broken relationships.
              </p>
                <h2>3. Developing Relationship with environment - Srishti</h2>
                <p class="cool-font">
                  Everything belongs to the Lord, so everything is inherently Divine Lord has assigned us our quota, which we should use in the spirit of stewardship - Yukta Vairagya. <br />
                  Everything to be used in the service of the Lord and his children, in the spirit of dettachment<br />
                  Purpose of this creation is to help everyone see the spiritual potential as a servant of God.<br />

                  One must know of all aspects of the material creation - science, geography, and so forth- alongside all details of the Supreme being, learned ina way appropriate to the particular development stages of each learner<br />
                  [Ref - isopanishad - mantras 1 and 14]
              </p>
                <h2>4. Developing Relationship with God - Parameshthi</h2>
                <p class="cool-font">
                  Anusmriti or constant find remembrance of God or Absolute Truth and nisvarth seva or selfless and pleasing service to God and fellow<br />
                  being [Ref - Isopanishad mantra 17, BG 8.5, SB 7.5.23]
              </p>


              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Philosophy;
