import lessonmodulesadmin from './Lesson_modules';
import Upload from './Upload';
import Approve from './Approve';

export {
  lessonmodulesadmin,
  Upload,
  Approve
};