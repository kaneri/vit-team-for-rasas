import React, { Component } from 'react';
import { Button, Label, Card, CardBody, CardImg, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from 'axios';
//import update from 'react-addons-update'; 
class Upload extends Component {
 
constructor(props) {
  super(props);
  this.appendInput=this.appendInput.bind(this);
  this.handleUpload=this.handleUpload.bind(this);
  this.onChanget=this.onChanget.bind(this);
  this.onChangef=this.onChangef.bind(this);
  this.onChangei=this.onChangei.bind(this);
  this.state = { titles: ['titles-0'], images: ['images-0'], files: ['files-0'], loaded:0, ufiles:[],uimages:[], utitles:[], usection: '', udescription: '' };
}


  handleUpload = () => {
    const data = new FormData();
    //console.log(this.state.uimages);
    for (let index = 0; index < this.state.uimages.length; index++) {
      data.append("images", this.state.uimages[index][0]);
      //console.log(this.state.uimages[index][0])
    }
    //console.log(this.state.uimages);

    for (let index = 0; index < this.state.ufiles.length; index++) {
      data.append("documents", this.state.ufiles[index][0]);
      //console.log(this.state.ufiles[index][0])
    }

    for (let index = 0;index < this.state.titles.length; index++) { 
      let utitles = this.state.utitles;     
      utitles[index]=(document.getElementById(this.state.titles[index]).value);
      this.setState({utitles});
      //console.log(this.state.utitles);
    }

    let usection = document.getElementById("sectionName").value;
    this.setState({usection});
        
    let udescription = document.getElementById("description").value;
    this.setState({udescription});

    /* for(var pair of imagedata.entries()) {
      //console.log(pair[0]+', '+JSON.stringify(pair[1]));
    }
 */
    let utitle = this.state.utitles;
    data.append('udescription',JSON.stringify(udescription));
    data.append('usection',JSON.stringify(usection));
    data.append('utitle',JSON.stringify(utitle));
    //console.log(data);
    axios
      .post('/api/admin/addsection', data,{
        
        onUploadProgress: ProgressEvent => {
          this.setState({
            loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
          })
        },
      })
      .then(res => {
        alert("Section Added Successfully! You can check it in Lesson Modules.")
      })
      .catch(err=>alert("Report Developer with:",err));
  }

onChanget(e) {
  let files=e.target.value;
  this.setState(prevState => ({ utitles: prevState.utitles.concat([files])
  }));
  //console.log(this.state.utitles);
}

onChangef(e) {
  let files=e.target.files;
  this.setState(prevState => ({ ufiles: prevState.ufiles.concat([files])
  }));
  //console.log(this.state.ufiles);
}

onChangei(e) {
  let files=e.target.files;
  this.setState(prevState => ({ uimages: prevState.uimages.concat([files])
  }));
  //console.log(this.state.uimages);
}

componentDidMount(){
  axios.defaults.withCredentials = true;
  axios.get('/api/admin/authenticate',{ withCredentials: true})
    .then(res=>{
      //console.log("logged in","wtf ");
      //this.getState();
      //return this.props.history.push('./Upload');
    })
    .catch(err=>{
      //console.log(err);
      return this.props.history.push('./login');
    })
    
}
appendInput() {
  var title = `titles-${this.state.titles.length}`;
  var image = `images-${this.state.images.length}`;
  var file = `files-${this.state.files.length}`;
  this.setState(prevState => ({ titles: prevState.titles.concat([title]), 
    images: prevState.images.concat([image]),
    files: prevState.files.concat([file])
  }));
  //console.log(this.state.titles);
  //console.log(this.state.images);
  //console.log(this.state.files);
}
  render() {
    return (
    <center>
      <div>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-2">
                <CardBody className="p-5">
                  <h1 style={{'color':'grey'}}><center>Add Section</center></h1>
					          <p>&nbsp; </p>
                  <InputGroup className="mb-3">
                    <Input type="text" placeholder="Section Heading" id="sectionName"/>
                  </InputGroup>
                  
				  <InputGroup className="mb-3">
                    <Input type="textarea" placeholder="Description" id="description"/>
                  </InputGroup>
				  
				 
                  <div id="dynamicInput">                  
                       {this.state.titles.map(title => <div><InputGroup className="mb-3"><Input key={title} id={title}  placeholder="Subsection Title"/></InputGroup>
                       <InputGroup className="mb-3">
                   <Row><Col md="4" lg="4" sm="4"><Label><b>Select Image</b></Label></Col><Col md="8" lg="8" sm="8"><Input type="file" id={`image-${title}`} onChange={this.onChangei} placeholder="Select Subsection Image" /></Col></Row>
                  </InputGroup>
                  <InputGroup className="mb-3">
                   <Row><Col md="4" lg="4" sm="4"><Label><b>Select File</b></Label></Col><Col md="8" lg="8" sm="8"><Input type="file" name={`file-${title}`} onChange={this.onChangef} placeholder="Select Subsection Image" /></Col></Row>
                  </InputGroup><hr/></div>)}                      
                   </div>
 
                  <Row><Col md="5" sm="5" lg="5"><Button outline color="info" onClick={this.appendInput}>Add Subsection</Button></Col></Row>
                  <br/>
                  <Row>
                    <Col md="4" sm="4" lg="4">
                     <Button style={{backgroundColor: "#87ceeb"}} onClick={this.handleUpload} block>Submit</Button>
                    </Col>
                    <Col md="8" sm="8" lg="8">
                     <Label>{this.state.loaded}%</Label>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
    
       
      </div></center>
    );
  }
}

export default Upload;
