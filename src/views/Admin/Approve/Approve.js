import React, { Component } from 'react';
import { Button, Label, Card, CardBody, CardFooter, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from 'axios';
import "./styleApprove.css";
// import { getMaxListeners } from 'cluster';

class Approve extends Component {
 
constructor(props) {
  super(props);
  this.state = { approveUser: '', rejectUser: [], users: [
    
  ]};
  this.rejectIt = this.rejectIt.bind(this);
  this.approveIt = this.approveIt.bind(this);

  this.approve = this.approve.bind(this);
  this.decline = this.decline.bind(this);
}



async approve(userToApprove){
  let approveUser=userToApprove;
  await this.setState({approveUser});
  //console.log(this.state,userToApprove);
  axios.post('/api/admin/approve', this.state)
    .then((res)=>{
      //console.log("Approved Successfully")
      alert("User has been approved successfully");
      window.location.reload();
    } )
    .catch(err=>{
      //console.log(err)
    });
  /*this.setState((prevState)=>{
    return{
      users: prevState.users.filter((user)=>{
        if(userToApprove != user.first)
        {
          return userToApprove != user.first;
        }
        else{
          prevState.approveUser.push(userToApprove);
          //console.log(this.state.approveUser);
        }
        
      })
    }
  })*/
}

async decline(userToDelete){
  let rejectUser=userToDelete;
  await this.setState({rejectUser});
  //console.log(this.state.rejectUser);
  axios.post('/api/admin/reject', this.state)
    .then((res)=>{//console.log("Rejected Successfully")
    alert("User has been rejected");
    window.location.reload();
  } )
    .catch(err=>{
      //console.log(err)
    });
  /*this.setState((prevState)=>{
    return{
      users: prevState.users.filter((user)=>{
        if(userToDelete != user.first)
        {
          return userToDelete != user.first;
        }
        else{
          prevState.rejectUser.push(userToDelete);
          //console.log(this.state.rejectUser);
        }
      })
    }
  })*/
}

approveIt(e) {
  let approveUser=e.target.name;
  this.setState({approveUser});
  //console.log(this.state.approveUser);
  axios.post('/api/admin/approve', this.state.approveUser)
    .then(
      //console.log("Approved Successfully") 
      )
    .catch(err=>{
      //console.log(err)
    });
}

rejectIt(e) {
  let rejectUser=e.target.name;
  this.setState({rejectUser});
  //console.log(this.state.rejectUser);
  axios.post('/api/admin/reject', this.state.rejectUser)
    .then(
      //console.log("Rejected Successfully") 
      )
    .catch(err=>{
      //console.log(err)
    });
}

async componentDidMount() {   
  axios.defaults.withCredentials = true;
  await axios.get('/api/admin/authenticate',{ withCredentials: true})
    .then(res=>{
      //console.log("logged in","wtf ");
      //this.getState();
      //return this.props.history.push('./Approve');
    })
    .catch(err=>{
      //console.log(err);
      //alert("User unauthorised")
      return this.props.history.push('./login');
      
    })
    



  axios.get('/api/admin/fetchUsers')
  .then(res=>{//console.log(res)
    let users = res;
    this.setState({users:res.data});
    //console.log(this.state);
  }
  )
  .catch(err=>{
  
  });
}
  render() {
    return (
      <div>
          <center>
            <h2 style={{'color':'grey'}}>Approve Users</h2>
          </center>
            
             
              
                
                
                  {/* for (let index = 0; index < this.state.users.length(); index++) {
                    <div>
                    <Card className="mx-2">
                <CardBody className="p-5">
                 <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>First Name</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{this.state.users[index].first_name}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Middle Name</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{this.state.users[index].middle_name}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Last Name</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{this.state.users[index].last_name}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Contact</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{this.state.users[index].contact}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Email</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{this.state.users[index].email}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Source of Connection</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{this.state.users[index].connection}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Usage Purpose</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{this.state.users[index].purpose}</Label>
                   </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button name={this.state.users[index].email} color="primary" onClick={this.approveIt}>Approve</Button>
                  <Button name={this.state.users[index].email} color="primary" onClick={this.rejectIt}>Reject</Button>
                </CardFooter>
              </Card>
                    <br/>
                    <br/>
                   </div>
                  }  */}
                  
                

        <List list={this.state.users} approve={this.approve} decline={this.decline}/>

      </div>
    );
  }
}

const List = (props)=>{
  return(
    <div>
    <center>
        {props.list.length === 0 && <div class="block"><p><b>No pending requests!</b></p></div>}
    </center> 
        {
          props.list.map((user)=><Pakshi name={user} approve={props.approve} decline={props.decline}/>)
        }   
    </div>
  )
}

const Pakshi=(props)=>{
  return(

    <div>
                    <Card className="mx-2">
                <CardBody className="p-5">
                 <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>First Name</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{props.name.first_name}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Middle Name</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{props.name.middle_name}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Last Name</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{props.name.last_name}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Contact</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{props.name.contact}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Email</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{props.name.email}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Source of Connection</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{props.name.connection}</Label>
                   </Col>
                  </Row>
                  <Row>
                   <Col md="4" lg="4" sm="4">
                    <Label>Usage Purpose</Label>
                   </Col>
                   <Col md="4" lg="4" sm="4">
                   <Label>{props.name.purpose}</Label>
                   </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button color="primary" onClick={()=>props.approve(props.name.email)}>Approve</Button>
                  <Button className="gap" color="primary" onClick={()=>props.decline(props.name.email)}>Reject</Button>
                </CardFooter>
              </Card>
                    <br/>
                    <br/>
                   </div>




    // <div class="user">
    // <Row>
    //   <Col xs="6" sm="6" lg="6">
    //   <div class="left">
    //     {props.name}  
    //   </div>
    //   </Col>
    //   <Col xs="6" sm="6" lg="6">
    //   <div class="right">
    //   <Button color="success" onClick={()=>props.approve(props.name)}>Approve</Button>
    //   <Button style={{'margin-left':'20px'}} color="danger" onClick={()=>props.decline(props.name)}>Decline</Button>
    //     {/* <Button style={{'margin-left':'20px'}}>Decline</Button> */}
    //   </div>
    //   </Col>

    //   </Row> 
    // </div>
  )
}
export default Approve;
