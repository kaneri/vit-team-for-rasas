import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import ScrollableAnchor from 'react-scrollable-anchor'
import axios from 'axios';
import{
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Button
} from 'reactstrap';

class Lesson_modules extends Component {
  constructor(props )
  {
    super(props);
    this.getState=this.getState.bind(this);
    this.state = { 
      /* Expected array from backend: { name : "tp",description : "this",subsection:[ { subname : 'sub1.1',image : "image1.1",document : "doc1.1"}, { subname : 'sub1.2',image : "image1.2",document : "doc1.2"} ]},{name : "tp1",description:"this1",subsection:[ { subname : 'sub2.1',image : "image2.1",document : "doc2.1"}, { subname : 'sub2.2',image : "image2.2",document : "doc2.2"} ]} */
      usection: []      
  };
  }
  getState() {
    axios.defaults.withCredentials = true;
    axios.get('/api/admin/section',{withCredentials: true})
    .then(res=>{//console.log(res)
      let usection = res.data;
      this.setState({usection});
    }
    )
    .catch(err=>{console.log(err,"section wtf")});
    
  }
  

  componentDidMount() {
   
    axios.get('/api/user/authenticate',{ withCredentials: true})
    .then(res=>{
      //console.log("logged in","wtf ");
      this.getState();
      return this.props.history.push('./Lesson_modules');
    })
    .catch(err=>{
      //console.log(err);
      return this.props.history.push('./login');
    })
  }
  render() {
    return (
      <div className="animated fadeIn" style={{ 'color': '#000000' }}>
        <Row>
          <Col xs="3" sm="3" lg="3"> 
          <Card style={{'background-color':'#78ccb8','padding':'2px','margin-bottom':'-30px'}}>
          <div style={{'max-height':'500px','overflow-y':'scroll'}}>
            <Card style={{'margin-bottom':'0px','margin-right':'0px'}}>
            <CardBody>
              
              <p><b><u>MODULES</u></b></p>
              <Menu usection = {this.state.usection}>
            {this.usection}
           </Menu>
              </CardBody>
            </Card>
            </div>
          </Card>           
          </Col>
          <Col xs="12" sm="12" lg="7">
            <Card style={{'margin-bottom':'0px'}}>
              <CardBody className="text-justify">
                <p>Lesson plans based on the strong foundation of scriptures - with creative teaching elements
                  including music,review games,activity pages,teaching pictures,hands on activities,videos and many
                  other resources, will help you teach your children with ease and great confidence.
                </p>
              </CardBody> 
            </Card>
            
            <Card style={{'background-color':'#78ccb8','margin-bottom':'0px'}}>
              <CardBody className="text-center" style={{'padding':'10px 0px 10px 0px'}}>
                <Row style={{'margin-bottom':'20px'}}>
                  <Col xs="0" sm="0" lg="1" style={{'padding-right':'0px'}}>
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./1.jpg')} width="110" height="110"/>
                    </div>
                    SUPPORT SCHOOL KIT
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./2.jpeg')} width="110" height="110"/>
                    </div>
                    LIFE SKILLS
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./3.jpg')} width="110" height="110"/>
                    </div>
                    KC PHILOSOPHY
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./4.jpg')} width="110" height="110"/>
                    </div>
                    KC CULTURE
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./5.jpg')} width="110" height="110"/>
                    </div>
                    VARNA TRAINING
                    </Col>
                    <Col xs="0" sm="0" lg="1" style={{'padding-left':'0px'}}>
                    </Col>
                </Row>

                <Row>
                  <Col xs="0" sm="0" lg="1" style={{'padding-right':'0px'}}>
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./6.jpg')} width="110" height="110"/>
                  </div>
                  ASHRAMA TRAINING
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./8_1.jpg')} width="110" height="110"/>
                  </div>
                  KALIYUGA SMART
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./8.jpg')} width="110" height="110"/>
                  </div>
                  ACADEMICS
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./9.jpg')} width="110" height="110"/>
                  </div>
                  VED SHIKSHA
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./10.jpg')} width="110" height="110"/>
                  </div>
                  64 ARTS
                  </Col>
                  <Col xs="0" sm="0" lg="1" style={{'padding-left':'0px'}}>
                  </Col>
                </Row>    
              </CardBody> 
            </Card>

          <Section usection = {this.state.usection}>
            {this.usection}
           </Section>
          
         

         
          </Col>     
          <Col xs="2" sm="2" lg="2">
            <Card>
              <CardHeader>
                RECENT UPDATES
              </CardHeader>  
              <CardBody>
                ...
              </CardBody>  
            </Card>

            <Card>
              <CardHeader>
                TOPICS
              </CardHeader>  
              <CardBody>
              ...                
              </CardBody>  
            </Card>

            <Card>
              <CardHeader>
                HELP
              </CardHeader>  
              <CardBody>
              ...
              </CardBody>  
            </Card>  
          </Col>
        </Row>

      </div>
    );
  }
}

function Menu(props){
  //console.log(props.usection,"this is props")
  const menu = 
  props.usection.map((e)=>
  <div>
<AnchorLink href={'#'+e.name}><span style={{'color':'red','font-size':'14px'}}><b>{e.name}</b></span></AnchorLink>
  <div style={{'margin-left':'20px','font-size':'12px','color':'green'}}>
  {e.subsection.map((e)=>
   <div>
   
   <span>{e.subname}</span><br/>
   {
     //console.log(e.subname+"this is bs")
    }
   </div>
  )}
  </div>  
  </div>  
  );
  
  return(
    <div>{menu}</div>
  )
}

function Section(props){
  
  const section = 
    props.usection.map((e)=>
    <div id="dynamicInput"> 
      <section id={e.name}>
      <Card style={{ 'margin-bottom': '0px' }} >
       <CardBody className="text-center">
         <h3 className="text-center">
           {e.name}
         </h3>
         <p><i><h6> {e.description}</h6></i></p>
         <p><h6>Please click the subsections for more details.</h6></p>
       </CardBody>
      </Card>
      <Card style={{'background-color':'#78ccb8','margin-bottom':'0px'}}>
        <CardBody className="text-center" style={{'padding':'10px 0px 10px 0px'}}>
          
            <Row>
          {e.subsection.map((e)=>     
                 //return(      {cin==}
                      
                <Col xs="12" sm="4" lg="4" style={{'padding':'0px'}}>
                  {
                    //console.log(e,"thua ia e")
                  } 
                  <div style={{'overflow':'hidden'}}>
                     <a href={URL.createObjectURL(new Blob([new Uint8Array( e.doc.data)],{type :'application/pdf'}))}  target="_self">
                    
                      <img src={e.image} width="110" height="150" alt='red'/>
                     </a>
                  </div>
                {e.subname}
                </Col>                 
            )
          }
        </Row>
          { 
            //console.log(e.subsection)
          }
        </CardBody>
        
      </Card>
      </section>
    </div>
  );
  
  return (
  <div>
    {section}     
  </div>
  )  
  
}



export default Lesson_modules;
