import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import axios from 'axios';
import{
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Button
} from 'reactstrap'


class Lesson_modules extends Component {
  constructor(props )
  {
    super(props);
    this.state = { 
      usection: []      
  };

  }

  componentDidMount() {
   
    axios.get('http//localhost:3000/api/admin/section')
    .then(res=>{//console.log(res)
      let usection = res;
      this.setState({usection});
    }
    )
    .catch(err=>{//console.log(err)});
  }
  render() {
    return (
      <div className="animated fadeIn" style={{ 'color': '#000000' }}>
        <Row>
          <Col>
            <Card>
              <CardBody>
                <Carousel autoPlay="true" infiniteLoop="true" showThumbs="false"  >
                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./1.jpg')} />
                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./2.jpeg')} />
                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./3.jpg')} />
                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./4.jpg')} />
                  </div>
                </Carousel>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="3" sm="3" lg="3">            
          </Col>
          <Col xs="12" sm="12" lg="7">
            <Card style={{'margin-bottom':'0px'}}>
              <CardBody className="text-justify">
                <p>Lesson plans based on the strong foundation of scriptures - with creative teaching elements
                  including music,review games,activity pages,teaching pictures,hands on activities,videos and many
                  other resources, will help you teach your children with ease and great confidence.
                </p>
              </CardBody> 
            </Card>
            
            <Card style={{'background-color':'#78ccb8','margin-bottom':'0px'}}>
              <CardBody className="text-center" style={{'padding':'10px 0px 10px 0px'}}>
                <Row style={{'margin-bottom':'20px'}}>
                  <Col xs="0" sm="0" lg="1" style={{'padding-right':'0px'}}>
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./1.jpg')} width="110" height="110"/>
                    </div>
                    SUPPORT SCHOOL KIT
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./2.jpeg')} width="110" height="110"/>
                    </div>
                    LIFE SKILLS
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./3.jpg')} width="110" height="110"/>
                    </div>
                    KC PHILOSOPHY
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./4.jpg')} width="110" height="110"/>
                    </div>
                    KC CULTURE
                    </Col>
                    <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                    <div style={{'overflow':'hidden'}}>
                      <img src={require('./5.jpg')} width="110" height="110"/>
                    </div>
                    VARNA TRAINING
                    </Col>
                    <Col xs="0" sm="0" lg="1" style={{'padding-left':'0px'}}>
                    </Col>
                </Row>

                <Row>
                  <Col xs="0" sm="0" lg="1" style={{'padding-right':'0px'}}>
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./6.jpg')} width="110" height="110"/>
                  </div>
                  ASHRAMA TRAINING
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./8.jpg')} width="110" height="110"/>
                  </div>
                  KALIYUGA SMART
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./1.jpg')} width="110" height="110"/>
                  </div>
                  ACADEMICS
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./2.jpeg')} width="110" height="110"/>
                  </div>
                  VED SHIKSHA
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./3.jpg')} width="110" height="110"/>
                  </div>
                  64 ARTS
                  </Col>
                  <Col xs="0" sm="0" lg="1" style={{'padding-left':'0px'}}>
                  </Col>
                </Row>    
              </CardBody> 
            </Card>

            <Card style={{'margin-bottom':'0px'}}>
              <CardBody className="text-center">
                <h5 className="text-center">
                  SUPPORT SCHOOL KIT
                </h5>
                <p>A list of activities that can be planned in a support school</p>
                <p>please click the links for more details.</p>
              </CardBody> 
            </Card>

            <Card style={{'background-color':'#78ccb8','margin-bottom':'0px'}}>
              <CardBody className="text-center" style={{'padding':'10px 0px 10px 0px'}}>
                <Row>
                  <Col xs="0" sm="0" lg="1" style={{'padding-right':'0px'}}>
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./5.jpg')} width="110" height="150"/>
                  </div>
                  ASSEMBLY
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./4.jpg')} width="110" height="150"/>
                  </div>
                  GEET
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./3.jpg')} width="110" height="150"/>
                  </div>
                  PANCHAKOSHA KHEL
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./2.jpeg')} width="110" height="150"/>
                  </div>
                  SHLOKA RECITATION
                  </Col>
                  <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                  <div style={{'overflow':'hidden'}}>
                    <img src={require('./1.jpg')} width="110" height="150"/>
                  </div>
                  KAHANI
                  </Col>
                  <Col xs="0" sm="0" lg="1" style={{'padding-left':'0px'}}>
                  </Col>
                </Row>  
              </CardBody>               
            </Card>

          <div id="dynamicInput">                  
           {            
             ()=>{
               for (let index = 0; index < this.state.usection.length; index++) {
                 /*jshint -W030 */
                 <div>
                 <Card style={{ 'margin-bottom': '0px' }}>
                   <CardBody className="text-center">
                     <h5 className="text-center">
                       {this.state.usection[index].name}
                     </h5>
                     <p>{this.state.usection[index].description}</p>
                     <p>please click the links for more details.</p>
                   </CardBody>
                 </Card>
                 
                 <Card style={{'background-color':'#78ccb8','margin-bottom':'0px'}}>
                 <CardBody className="text-center" style={{'padding':'10px 0px 10px 0px'}}>
                   {
                     () => {
                     let counter = 0;
                     for (let i = 0; i < Math.ceil(this.state.usection[index].doc.length/4); i++) {
                      <Row>
                      <Col xs="0" sm="0" lg="1" style={{'padding-right':'0px'}}>
                      </Col> 
                      {
                        () => {  
                        for (let indexInner = 0; indexInner < 4; indexInner++) {
                          <Col xs="6" sm="4" lg="2" style={{'padding':'0px'}}>
                            <div style={{'overflow':'hidden'}}>
                             <a href={this.state.usection[index].doc[counter]} download={this.state.usection[index].doc[counter]}>
                              <img src={this.state.usection[index].image[counter]} width="110" height="150"/>
                             </a>
                            </div>
                            {this.state.usection[index].subname[counter]}}
                            {counter = counter + 1}
                          </Col>                           
                        } /*For loop*/                            
                        } /*Outer block*/ 
                       }                                       
                      <Col xs="0" sm="0" lg="1" style={{'padding-left':'0px'}}>
                      </Col>
                    </Row>                       
                   }
                  }
                  }                   
                 </CardBody>                  
               </Card>
               </div>
               }/*For loop*/
             
              }/*Outer*/
            } 
          </div>

         
          </Col>     
          <Col xs="2" sm="2" lg="2">
            <Card>
              <CardHeader>
                RECENT UPDATES
              </CardHeader>  
              <CardBody>
                xxxxxxxxxxxxxxx
              </CardBody>  
            </Card>

            <Card>
              <CardHeader>
                TOPICS
              </CardHeader>  
              <CardBody>
                  xxxxxxxxxxxxxxx                
              </CardBody>  
            </Card>

            <Card>
              <CardHeader>
                HELP
              </CardHeader>  
              <CardBody>
                xxxxxxxxxxxxxxx
              </CardBody>  
            </Card>  
          </Col>
        </Row>

      </div>
    );
  }
}

//export default Lesson_modules;
