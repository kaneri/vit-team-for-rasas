import React, { Component } from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import "./register.css";
import axios from 'axios'; 


class Register extends Component {
  constructor(props){
    super(props);
    this.submitreg = this.submitreg.bind(this); 
    this.handle_change = this.handle_change.bind(this);
    this.selection = this.selection.bind(this);
    this.check = this.check.bind(this);
    this.state = {
      "purpose": "",
      check: true
    }
  }
  handle_change(e) {
    this.setState({
      [e.target.name]: e.target.value
  })
}
check() {
  this.setState({check:false});
}
selection(e) {
  //console.log(e.target.id);
  let purpose = e.target.id;
  this.setState({
   purpose
})
}

submitreg()
   {
     //console.log(this.state);
     axios.post('/api/user/register', this.state)
          .then((result) => {
            //console.log(result)
            alert("You would be able to login once Admin approves your registration.")
          })
          .catch(err=>alert("Missing/Incorrect Values or Email ID already taken"));
   } 

  render() {
    return (
      
      <div className="app align-items-center" >
        <Container >
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>

                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="fa fa-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" onChange={this.handle_change} placeholder="First Name" name="first_name"/>
                  </InputGroup>

                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="fa fa-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" onChange={this.handle_change} placeholder="Middle Name" name="middle_name"/>
                  </InputGroup>
       

                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="fa fa-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" onChange={this.handle_change} placeholder="Last Name" name="last_name"/>
                  </InputGroup>
                  
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="fa fa-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" onChange={this.handle_change} placeholder="Password" name="password"/>
                  </InputGroup>

                    <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText id="mobile">
                        <i className="fa fa-mobile" ></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" onChange={this.handle_change} placeholder="Contact" name="contact"/>
                  </InputGroup>
                  
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="fa fa-envelope" ></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" onChange={this.handle_change} placeholder="Email" name="email"/>

                  </InputGroup>
                  
                   <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="fa fa-users" ></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" onChange={this.handle_change} placeholder="Source of connection" name="connection"/>
                  </InputGroup>
              
                  <InputGroup className="mb-3">
                  <Row>
                    <Col xs="12" sm="12" lg="12">
                  		<h6 id="typeofuse">How would you use contents of the application ?</h6>
                  	</Col>
                  </Row>
                  </InputGroup>

                  <InputGroup className="mb-3">
                  <Row>
                    <Col sm='12' xs='12' md='6' lg='6'>
                  	<InputGroupText id="professional">
                  		<Input type="radio" id="Professional" name="opt" onClick={this.selection}/>
                  		Professional
                  	</InputGroupText>
                    </Col>
                  	<Col sm='12' xs='12' md='6' lg='6'>
                  	<InputGroupText id="personal">
                  		<Input type="radio" id="Personal" name="opt" onClick={this.selection}/>
                  		Personal
                  	</InputGroupText>
                    </Col>
                    </Row>
                  </InputGroup>
              
              <br/>

                  <InputGroup className="mb-3">
                 
                  	   <Row>
                    	<Col xs="12" sm="12" lg="12">
                  			<p id="terms">Terms: Contents of the application have RASAS copyrights. Use them wisely for good cause. 
                  			Redistribution of the contents for exchange of charges is strictly prohibited.
                  			</p>
                  		</Col>
                  		</Row>
                  
                  </InputGroup>


                  <InputGroup className="mb-3">
                  		<Input type="checkbox" id="agreeterms" onClick={this.check}/>
                  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I agree to the above terms. 
                  </InputGroup>
                  <br/>

                  <Button color="success" onClick={this.submitreg} disabled={this.state.check}>Create Account</Button>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Register;
