import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./dashboard.css";

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardBody>
                <Carousel autoPlay="true" infiniteLoop="true" showThumbs="false"  >
                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./slideshow-1.jpg')} />

                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img  src={require('./slideshow-2.jpg')} />

                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./slideshow-3.jpg')} />

                  </div>

                  <div style={{ 'width': '100%', 'height': '100%' }} > <img src={require('./slideshow-4.jpg')} />

                  </div>
                </Carousel>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>

          <Col xs="12" sm="12" lg="4">
            <Card>
              <CardHeader className="card-head">
                <b><i >Vision</i></b>
              </CardHeader>
              <CardBody className="back">
                <div class="row">

                  	<img class="card-img-top rounded" height="250px" src={require('./vision.jpg')} alt="Card image cap" />

                </div>
              </CardBody>
            </Card>
            <Card>
              <CardHeader className="card-head">
                <b><i >UPCOMING EVENTS</i></b>
              </CardHeader>
              <CardBody>
                <div class="cool-font">
                  <marquee direction="up" scrollamount="4">
                      <b>Workshop for Teachers</b><br/><br/>
                  </marquee>
                </div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="12" lg="8">
            <Card>
              <CardHeader className="card-head">
                <b><i >FOR WHOM?</i></b>
              </CardHeader>
              <CardBody>
                <img class="card-img-top rounded" src={require('./schoolFor.jpg')} alt="Card image cap" />
                <div class="row">
                  <div class="col-11 text-justify cool-font"><br />
                    Many parents are choosing to home school their children or
                    take up the charge of training the children in their own
                    hands irrespective of quality of education being offered
                    through public / private schools.<br />
                    <ul>
                      <li>They are dissatisfied with the education options that are
                        available</li>
                      <li>They may have personal religious beliefs like we have
                      Krishna consciousness as our practice and education, philosophy
                        based on it, that we may want to impart to our children.</li>
                      <li>There is a belief that children are not progressing holistically
                      by the regular school structure system, which is more of a
                      factory creating compliance oriented workforce for the
                        corporate industry in modern times.</li>
                    </ul>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardHeader className="card-head">
                <b><i >ALTERNATE EDUCATION</i></b>
              </CardHeader>
              <CardBody>
                <Row>
                <Col sm="6" lg="6" md="6">
                  <div class="col-11 text-justify cool-font"><br />
                          To support the parents who want alternative education
                          But parents sometimes may not have an education degree
                    / background.<br /><br />
                          <b>This WEBSITE offers :</b>
                          <ul>
                            <li>Some basic education for parents - on how to educate a
                            child. So we have philosophy of education, basic practices,
goals of education clarified at the beginning.</li>
                            <li>Website offers practical curriculum, some lesson plans,
                            assessments, accountability and resources for further
coaching.</li>
                            <li>It gives tips on - Time table options</li>
                            <li>Website to incorporate the Krishna Conscious
                            education philosophy based on the teaching s of our
                            Founder Acharya, Srila Prabhupada - How to educate
children - Key tenets or guidelines.</li>
                            <li>This website will also offer a networking platform, for
                            people to share, care and offer quality education
together to their children.</li>
                            <li>Website content is developed taking help from other
                            secular methods like Blooms Taxonomy etc. thus the
approach is not just one sided.</li>
                          </ul>
                        </div>
                </Col>
                <Col sm="6" lg="6" md="6">
                      <img class="card-img-top rounded" src={require('./alternate.jpg')} alt="Card image cap" />
                </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardHeader className="card-head">
                <b><i >OUR MISSION</i></b>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="4" sm="4" lg="4">
                    <img class="card-img-top rounded" src={require('./mission1.jpg')} alt="Card image cap" />
                  </Col>
                  <Col xs="8" sm="8" lg="8">
                    <div class="row">
                      <div class="col-11 text-justify cool-font">
                        <b>TO FORM & MAINTAIN GOOD HABITS</b><br />
                        Diet, rest, exercise, yoga, japa, sastra study, mantra recitation, cleanliness, hygiene, kahani, khel, gana.
                          </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4" sm="4" lg="4">
                    <img class="card-img-top rounded" src={require('./mission2.jpg')} alt="Card image cap" />
                  </Col>
                  <Col xs="8" sm="8" lg="8">
                    <div class="row">
                      <div class="col-11 text-justify cool-font">
                        <b>CREATE SOUL SANCTUARY SPACES</b><br />
                        Providing children a consistent, happy, positive, growing environment.
                          </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4" sm="4" lg="4">
                    <img class="card-img-top rounded" src={require('./mission3.jpg')} alt="Card image cap" />
                  </Col>
                  <Col xs="8" sm="8" lg="8">
                    <div class="row">
                      <div class="col-11 text-justify cool-font">
                        <b>FACILITATE CHARACTER DEVELOPMENT</b><br />
                        By forming and maintaining good habits, eventually we will form good character
and through these children produce good social contribution.
                          </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4" sm="4" lg="4">
                    <img class="card-img-top rounded" src={require('./mission4.jpg')} alt="Card image cap" />
                  </Col>
                  <Col xs="8" sm="8" lg="8">
                    <div class="row">
                      <div class="col-11 text-justify cool-font">
                        <b>GIVE SUSTAINED GOOD ASSOCIATION</b><br />
                        Give children, especially between age group 6-17, sustained good association, good
role models, quality peer relationships, a care giving support.
                          </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4" sm="4" lg="4">
                    <img class="card-img-top rounded" src={require('./mission5.jpg')} alt="Card image cap" />
                  </Col>
                  <Col xs="8" sm="8" lg="8">
                    <div class="row">
                      <div class="col-11 text-justify cool-font">
                        <b>PROVIDE CULTURAL SYNC EXPERIENCE</b><br />
                        Non-jerky experience when children interact with parents (home environment),
interact with peers, school environment etc.
                          </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4" sm="4" lg="4">
                    <img class="card-img-top rounded" src={require('./mission6.jpg')} alt="Card image cap" />
                  </Col>
                  <Col xs="8" sm="8" lg="8">
                    <div class="row">
                      <div class="col-11 text-justify cool-font">
                        <b>EQUIP & ENGAGE OUR KEY AUDIENCE</b>
                        <ul>
                          <li>Children (Age 6-17)</li>
                          <li>Teachers (Brahmachari and Grihasthas interested in children's education)</li>
                          <li>Parents</li>
                          <li>Any agency that support our activities will be our stakeholder</li>
                        </ul>
                      </div>
                    </div>
                  </Col>
                </Row>

	            <Row>
	              	<Col xs="4" sm="4" lg="4">
	                <img class="card-img-top rounded" src={require('./mission7.jpg')} alt="Card image cap" />
	              	</Col>
	              	<Col xs="8" sm="8" lg="8">
	                <div class="row">
	                  <div class="col-11 text-justify cool-font">
	              		<b>Train in - COMBO of 3 ELEMENTS -</b>
                        <ul>
                          <li>Krishna Consciousness</li>
                          <li>Varnashram</li>
                          <li>Being Worldy smart / relevant</li>
                        </ul>
	                  </div>
	                </div>
	            	</Col>
	            </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardHeader><b><i style={{'color':'white'}}><center>Process</center></i></b></CardHeader>
              <CardBody>
                <Row>
                  <Col xs="6" sm="6" lg="4">
                    <figure class="figure">
                      <img src={require("./parivarshiksha.jpg")} class="figure-img img-fluid rounded imageActivated" alt="Parivar Shiksha" />
                      <figcaption class="figure-caption cool-font">PARIVAR SHIKSHA</figcaption>
                    </figure>
                  </Col>
                  <Col xs="6" sm="6" lg="4">
                    <figure class="figure">
                      <img src={require("./panchakosa.jpg")} class="figure-img img-fluid rounded imageActivated" alt="Panchakosha" />
                      <figcaption class="figure-caption cool-font">PANCHAKOSHA</figcaption>
                    </figure>
                  </Col>
                  <Col xs="6" sm="6" lg="4">
                    <figure class="figure">
                      <img src={require("./anandakosha.jpg")} class="figure-img img-fluid rounded imageActivated" alt="Anandamaya" />
                      <figcaption class="figure-caption cool-font">ANANDAMAYA</figcaption>
                    </figure>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter></CardFooter>
            </Card>
          </Col>
        </Row> */}
      </div>
    );
  }
}

export default Dashboard;
