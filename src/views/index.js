import Assess from './Assess';
import Dashboard from './Dashboard';
import { Login, Page404, Page500, Register } from './Pages';
import Forgot from './Pages/Login/Forgot'
import {Timetable, Process} from './';
import Lesson_modules from './Lesson_modules';
import lessonmodulesadmin from './Admin/Lesson_modules';
import Upload from './Admin/Upload';
import Approve from './Admin/Approve';

export {
  Timetable,
  Process,
  Page404,
  Page500,
  Register,
  Login,
  Forgot,
  Assess,
  Dashboard,
  Lesson_modules,
  lessonmodulesadmin,
  Upload,
  Approve
};

