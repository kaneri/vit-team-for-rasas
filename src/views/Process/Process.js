import React, { Component } from 'react';
import ReactDOM from 'react-dom';

 
import classNames from 'classnames';
import {
        Row,
        Col,
        Card,
        CardGroup,
        CardImg,
        CardHeader,
        CardDeck,
        CardBody,
        CardText
            } from 'reactstrap'
import { rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities'
import "./style1.css";
import AnchorLink from 'react-anchor-link-smooth-scroll'


class Process extends Component {
  render() {
    return (
      //SECTION 1
    <div className="animated fadeIn">
          <div id="heading">
            <Row>
              <Col xs="10" sm="2" lg="2">
                <div id="logo">
                  <Card className="border-0">
                    <CardImg height="150px" src={require('./notes.jpeg')}/>
                  </Card>
                </div>
              </Col>
              <Col xs="10" sm="10" lg="10">
                <div id="heading-text">
                  <Card className="border-0">
                    <h3 class="card-head">EXPLORE OUR PROCESS</h3>
                    <h5 class="cool-font">Just like an expert Musician uses 7 basic notes to compose a variety of beautiful occassions
                     so does a Teacher blend these basic elements - steps & techniques to understand the student and
                     devise tailormade lesson plans as required.</h5>
                  </Card>
                </div>
              </Col>
            </Row>
          </div>

          <div id="cards">
            <Row>
              <Col xs="12" sm="12" lg="12">
                  <CardDeck>
                    <Card>
                      <CardImg top width="100%" src={require('./knowingIndividual.jpg')} />
                      <div id="card-body">
                        <div class="text-center card-head" id="card-head">
                          <h3>KNOWING THE INDIVIDUAL</h3>
                          <h3>?</h3>
                        </div>
                        <div id="card-text" class="text-justify cool-font">
                          <h5>Knowing the individual - to know the psychological nature of every student. This can be done...</h5>
                        </div>
                        <h4 class="text-center cool-font"><AnchorLink offset='100' href='#section1'><span style={{'color': 'WHITE'}}>KNOW MORE</span> </AnchorLink><i className="fa-arrow"></i></h4>
                      </div>
                    </Card>
                    <Card>
                      <CardImg top width="100%" src={require('./buildingRapport.jpg')} />
                      <div id="card-body">
                        <div class="text-center card-head" id="card-head">
                          <h3>BUILDING RAPPORT WITH THE CHILD</h3>
                          <h4>(MMOT)</h4>
                        </div>
                        <div id="card-text" class="text-justify cool-font">
                          <h5>How to build a good rapport and relationship with the child?</h5>
                        </div>
                        <h4 class="text-center cool-font"><AnchorLink offset='100' href='#section2'><span style={{'color': 'WHITE'}}>KNOW MORE</span> </AnchorLink> <i className="fa-arrow"></i></h4>

                      </div>
                    </Card>
                    <Card>
                      <CardImg top width="100%" src={require('./settingClassrules.jpg')} />
                      <div id="card-body">
                        <div class="text-center card-head" id="card-head">
                          <h3>SETTING THE CLASSROOM RULES</h3>
                          <h3>?</h3>
                        </div>
                        <div id="card-text" class="text-justify cool-font">
                          <h5></h5>
                        </div>
                        <h4 class="text-center cool-font">
                        <AnchorLink offset='100' href='#section3'><span style={{'color': 'WHITE'}}>KNOW MORE</span></AnchorLink>
                        <i className="fa-arrow"></i>
                        </h4>
                      </div>
                    </Card>
                  </CardDeck>
              </Col>
            </Row>
          </div>

          <div id="cards">
                <Row>
                  <Col xs="12" sm="12" lg="12">
                      <CardDeck>
                        <Card>
                          <CardImg top width="100%" src={require('./howLearners.jpg')} />
                          <div id="card-body">
                            <div class="text-center card-head" id="card-head-big">
                              <h3>HOW LEARNERS LEARN?</h3>
                              <h4><b>TEACHING-LEARNING</b></h4>
                            </div>
                            <div id="card-text" class="text-justify cool-font">
                              <h5>
                                <ul>
                                  <li>Hearing from authority</li>
                                  <li>Examples of others</li>
                                  <li>Demonstration...</li>
                                </ul>
                              </h5>
                            </div>
                            <h4 class="text-center cool-font"><AnchorLink offset='100' href='#section4'><span style={{'color': 'WHITE'}}>KNOW MORE</span> </AnchorLink> <i className="fa-arrow"></i></h4>
                          </div>
                        </Card>
                        <Card>
                          <CardImg top width="100%" src={require('./strategies.jpg')} />
                          <div id="card-body">
                            <div class="text-center card-head" id="card-head-big">
                              <h3>WHAT STRATEGIES TEACHERS USE?</h3>
                            </div>
                            <div id="card-text" class="text-justify cool-font">
                              <h5>Creating proper setting for learning & practice - <b>Anushthana</b></h5>
                              <h5>Encourage & facilitate learning</h5>
                            </div>
                            <h4 class="text-center cool-font"><AnchorLink offset='100' href='#section5'><span style={{'color': 'WHITE'}}>KNOW MORE</span> </AnchorLink><i className="fa-arrow"></i></h4>
                          </div>
                        </Card>
                        <Card>
                          <CardImg top width="100%" src={require('./5steps.jpg')} />
                          <div id="card-body">
                            <div class="text-center card-head" id="card-head-big">
                              <h3>5 STEPS FOR EACH LESSON PLAN ?</h3>
                             
                            </div>
                            <div id="card-text" class="text-justify cool-font">
                              <h5><ul><li>Begin with the attention grabbing and interest generating introduction</li></ul></h5>
                              <h5><ul><li><b>Sambandha</b></li></ul></h5>
                            </div>
                            <h4 class="text-center cool"><AnchorLink offset='100' href='#section6'><span style={{'color': 'WHITE'}}>KNOW MORE</span> </AnchorLink><i className="fa-arrow"></i></h4>
                          </div>
                        </Card>
                      </CardDeck>
                  </Col>
                </Row>
              </div>

              {/* SECTION 2 */}

            <div id="a">

              <div>
                <section id='section1'>
                <Row style={{'margin-bottom': '0px'}}>
                  <Col xs="12" sm="12" lg="12" style={{'margin-bottom': '0px'}}>
                    <div id="aspects" class="text-center card-head" style={{'margin-bottom': '0px'}}>
                      <h3>KNOWING THE INDIVIDUAL</h3>
                    </div>
                  </Col>
                </Row>
                </section>
              </div>

              <div class="cool-font">
                <Row>
                  <Col xs="12" sm="12" lg="12">
                    <div id="knowing-individual-head1" class="cool-font">
                      <h5>Teacher to know the psychological nature of every student.
                      This can be done using help of Astrology, Ayurveda, Enneagram, Nine
                    types of Intelligence</h5>
                    </div>
                    <div id="knowing-individual-head2" class="cool-font">
                      <Row>
                        <Col xs="12" sm="12" lg="12">
                          <Card transparent className="border-0" id="trans">
                            <Row>
                              <Col xs="4" sm="4" lg="4">
                                <h5>+ ASTROLOGICAL DETAILS</h5>
                              </Col>
                              <Col xs="8" sm="8" lg="8">
                                <p>(rashi, svabhav, jyotish)</p>
                              </Col>
                            </Row>
                          </Card>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" sm="12" lg="12">
                          <Card transparent className="border-0" id="trans">
                            <Row>
                              <Col xs="4" sm="4" lg="4">
                                <h5>+ PHYSICAL NATURE</h5>
                              </Col>
                              <Col xs="8" sm="8" lg="8">
                                <p>(Ayurvedic constitution)</p>
                              </Col>
                            </Row>
                          </Card>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" sm="12" lg="12">
                          <Card transparent className="border-0" id="trans">
                            <Row>
                              <Col xs="4" sm="4" lg="4">
                                <h5>+ PERSONALITY TYPE</h5>
                              </Col>
                              <Col xs="8" sm="8" lg="8">
                                <p>(Enneagram)</p>
                              </Col>
                            </Row>
                          </Card>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12" sm="12" lg="12">
                          <Card transparent className="border-0" id="trans">
                            <Row>
                              <Col xs="4" sm="4" lg="4">
                                <h5>+ & TYPE OF INTELLIGENCE</h5>
                              </Col>
                              <Col xs="8" sm="8" lg="8">
                                <p>(9 types of Intelligence as given by Howard Gardener)</p>
                              </Col>
                            </Row>
                          </Card>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>


              <div id="outer-tab">
                <Row>
                  <Col xs="12" sm="12" md="12" lg="6">
                    <div>
                      <Card>
                        <CardImg src={require('./personalityTypes.jpg')}/>
                      </Card>
                    </div>
                  </Col>
                  <Col xs="12" sm="12" md="12" lg="6">
                    <div>
                      <Card>
                        <CardImg src={require('./enneagram.jpg')}/>
                      </Card>
                    </div>
                  </Col>
                </Row>
              </div>
              <div class="cool">
                <Row>
                  <Col xs="12" sm="12" lg="12">
                    <div id="random-div">
                      <Card id="trans-1" className="border-0 cool-font">
                        <h5>+ PERSONALITY TYPE - The Enneagram refers to the 9 different styles, with each representing a worldwide
                        and archetype that resonates with the way people think, feel and act in relation to the world, others and
                      themselves.</h5>
                      </Card>
                  </div>
                  </Col>
                </Row>
              </div>

              <div>
                <Row>
                  <Col xs="12" sm="12" lg="7" md="12" style={{'padding-right': '0px'}}>
                    <Card className="border-0" id="trans">
                      <CardImg  src={require('./intelligence.jpg')}/>
                    </Card>
                  </Col>
                  <Col xs="12" sm="12" lg="5" style={{'padding-left': '0px'}}>
                    <div style={{'padding': '5px 0 0 10px'}}>
                    <div id="intelligence" class="cool">
                      <Card className="border-0 card-head" id="trans">
                          <h4>+9 TYPES OF INTELLIGENCES</h4>
                      </Card>
                    </div>
                    <div id="intelligence-text" class="cool-font">
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Naturalist</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(nature smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Musical</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(sound smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Logical-mathematical</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(number/reasoning smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Existential</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(life smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Interpersonal</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(people smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Bodily-kinesthetic</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(body smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Linguistic</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(word smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Intra-personal</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(self smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Card transparent className="border-0" id="trans">
                          <Row>
                            <Col xs="6" sm="6" lg="6">
                              <ul>
                                <li><b>Spatial</b></li>
                              </ul>
                            </Col>
                            <Col xs="6" sm="6" lg="6">
                              <p>(picture smart)</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                  </div>
                  <div id="outer-box" class="cool">
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <div id="intelligence-text-2">
                          <Card className="border-0" id="trans">
                            <p><b>Students learn in ways that are identifiably distinctive
                            as every student possesses different kinds of minds and therefore learn, remember,
                          perform and understand in different ways. </b></p>
                            <p><b>LEARNING STYLES ARE DIFFERENT CASE TO CASE.</b></p>
                          </Card>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
                  </Col>
                </Row>
              </div>
            </div>

            {/* SECTION 2 */}
<br></br><br></br>
           <div>
              <div>
              <section id='section2'>
                <Row style={{'margin-bottom': '0px'}}>
                  <Col xs="12" sm="12" lg="12" style={{'margin-bottom': '0px'}}>
                    <div id="plain-white" class="text-center card-head" style={{'margin-bottom': '0px'}}>
                    <h3>BUILDING RAPPORT WITH THE CHILD (MMOT)</h3>
                    </div>
                  </Col>
                </Row>
              </section>
              </div>
              <div>
                <Row>
                    <Col xs="12" sm="12" lg="12" style={{'padding-right': '10'}}>
                      <div id="mmot">
                        <Card className="border-0" id="trans">
                          <CardImg src={require('./mmot.jpg')}/>
                        </Card>
                      </div>
                    </Col>
                  </Row>
              </div>
           </div>

            {/* SECTION 3 */}
<br></br><br></br>
            <div>
              <section id='section3'>
              <Row>
                <Col xs="12" sm="12" lg="12">
                  <div id="classroom-rules" class="text-center card-head">
                    <h3>SETTING THE GROUND / CLASSROOM RULES</h3>
                  </div>
                  <ul class="cool-font">
                   <li><b>Anushthana</b></li>
                   <p class="cool-font">
                     <ul>
                        <li><b>Punctuality</b> - Start and end activities on time.</li>
                        <li><b>Tidy/CLeanliness</b> - Keep your personal and classroom belongings tidy.</li>
                        <li><b>Compassion/Politeness</b> - Respect all.Treat everyone and everything the way you would like to be treated.Be nice to everybody.</li>
                        <li><b>Truthfulness</b></li>
                        <li><b>Self control</b> - Keep your hands and legs to yourself.</li>
                     </ul>  
                   </p>
                   <li><b>Adhyayana</b></li>
                   <p class="cool-font">
                     <ul>
                        <li><b>Attention/listening/Observe</b> - Be a curious listener.</li>
                        <li><b>Swa adhyaya</b> - Be responsible for your own learning.</li>
                        <li><b>Obidience</b> - Follow directions of teacher.</li>
                     </ul>  
                   </p>
                   <li><b>Anusandhana</b></li>
                   <p class="cool-font">
                     <ul>
                        <li><b>Abhyasa</b> - Smart work/practice.</li>
                        <li>Raise hand to contribute/ask.</li>
                        <li>Collaboration, not competition.</li>
                     </ul>  
                   </p>
                   <li><b>Andhyaapana/Seva</b></li>
                   <p class="cool-font">
                     <ul>
                        <li><b>Generosity</b> - Share what you have and what you know.</li>
                        <li><b>Courteousness</b> -Serve/care for others as we are one loving family.</li>
                     </ul>  
                   </p>
                  </ul>
                </Col>
              </Row>
              </section>
            </div>

            {/* SECTION 4 */}

            <div>
              <div>
                <section id='section4'>
                <Row style={{'margin-bottom': '0px'}}>
                  <Col xs="12" sm="12" lg="12" style={{'margin-bottom': '0px'}}>
                    <div id="classroom-rules" class="text-center card-head" style={{'margin-bottom': '0px'}}>
                      <h3>HOW LEARNERS LEARN</h3>
                    </div>
                  </Col>
                </Row>
                </section>
              </div>
              <div class="cool">
                <Row style={{'margin-bottom': '0'}}>
                  <Col xs="12" sm="12" lg="12" style={{'margin-bottom': '0px'}}>
                    <div id="classroom-rules-2" class="cool-font">
                      <ul>
                        <li>Hearing from authority</li>
                        <li>Examples of others</li>
                        <li>Demonstration</li>
                        <li>Direct, practical experience</li>
                        <li>Progressively and developmentally</li>
                        <li>By developing good character, the most important basis of learning-
                        by controlling the urges and seeking the blessings of the teachers and seniors to help us
                        properly contemplate of the learning we have gathered</li>
                      </ul>
                      <div id="classroom-rules-3">
                        <h4><b>4 ASPECTS OF LEARNING</b></h4>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
              <div>
                <Row style={{'margin-bottom': '0'}}>
                  <Col xs="12" sm="12" lg="12">
                    <Card className="border-0" id="trans">
                      <CardImg  src={require('./4aspects.jpg')}/>
                    </Card>
                  </Col>
                </Row>
              </div>

            {/*SECTION 5*/}
    <br></br><br></br>
              <div>
                  <div id="teach-learn-header">
                    <section id='section5'>
                    <Row>
                      <Col>
                          <div  class="text-center card-head" style={{'margin-bottom': '0px'}}>
                          <h3 >METHODS OF TEACHING LEARNING</h3>
                          </div>
                      </Col>
                    </Row>
                    </section>
                  </div>
                  <div id="teach-learn">
                  <Row>
                      <Col xs="12" sm="12" lg="5">
                        <div id="teach-learn-img">
                          <Card className="border-0" id="trans">
                            <CardImg src={require('./mtl.jpg')}/>
                          </Card>
                        </div>
                      </Col>
                      <Col xs="12" sm="12" lg="7">
                        <div style={{'padding': '20px 0 0 20px'}} class="cool-font">
                          <Card className="border-0" id="trans">
                            <Row>
                              <Col xs="12" sm="12" lg="12">
                                <div id="head-long">
                                  <p><b>1. SRAVAN-</b></p>
                                </div>
                                <div id="head-long-text">
                                  <p id="bold">Proper hearing, recitation, discussion, imitation, lecturing, reading, repetation</p>
                                </div>
                              </Col>
                              <Col xs="12" sm="12" lg="12">
                                <div id="head-long">
                                  <p><b>2. MANAN-</b></p>
                                </div>
                                <div id="head-long-text">
                                  <p id="bold">Recognizing, inferring, identifying, retrieving, explaining,
                                  matching, classifying, locating, listing, critiquing, hypothesizing,
                                judging, defending, prioritizing, analysis, synthesis, inductive-deductive, roleplay,
                              case study, concluding, collaborating, assessing, deducing, surveying, examining, outlining, differentiating,
                            reviewing, editing, comparing and contrasting, summarizing.</p>
                                </div>
                              </Col>
                              <Col xs="12" sm="12" lg="12">
                                <div id="head-long">
                                  <p><b>3. SMARAN-</b></p>
                                </div>
                                <div id="head-long-text">
                                  <p id="bold">Problem solving, play way, project method, laboratory techniques</p>
                                </div>
                              </Col>
                              <Col xs="12" sm="12" lg="12">
                                <div id="head-long">
                                  <p><b>4. SAMPATTI-</b></p>
                                </div>
                                <div id="head-long-text">
                                  <p id="bold">Fond remembrance of Lord, pleasing and selfless service, utility of the learned subject matter, introspection,
                                  deep contemplation, worshipful prayers</p>
                                </div>
                              </Col>
                            </Row>
                          </Card>
                        </div>
                      </Col>
                  </Row>
                </div>

                 {/*SECTION 6*/}
   <br></br><br></br>
                <div>
                  <section id='section6'>
                  <Row style={{'margin-bottom': '0px'}}>
                    <Col xs="12" sm="12" lg="12" style={{'margin-bottom': '0px'}}>
                      <div id="plain-white" class="text-center card-head" style={{'margin-bottom': '0px'}}>
                        <h3>5 STEPS TO MAKE A QUICK LESSON PLAN</h3>
                      </div>
                    </Col>
                  </Row>
                 </section>
                </div>
                <div id="last-section">
                  <Row>
                    <Col xs="12" sm="12" lg="5" style={{'padding-right': '0'}}>
                      <div id="last-img">
                        <Card className="border-0" id="trans">
                          <CardImg src={require('./anushtan.jpg')}/>
                        </Card>
                      </div>
                    </Col>
                    <Col xs="12" sm="12" lg="7" style={{'padding-left': '0'}}>
                      <div id="last-img-2">
                        <Card className="border-0" id="trans">
                          <CardImg src={require('./lesson-plan.jpg')}/>
                        </Card>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>



    </div>
    );
  }
}

export default Process;
