import React, { Component } from 'react';

import{
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  CardImg
} from 'reactstrap'
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';

import './back.css';
import AnchorLink from 'react-anchor-link-smooth-scroll';

class Timetable extends Component {
  render() {
    return (
      <div className="animated fadeIn" >
        <img class="img-fluid" src={require('./timetablechart.jpg')}/>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <div id="time-table-head" class="text-center card-head">
              <h1>TIME TABLE OPTIONS - click to see</h1>
              <h5 style={{'margin-bottom': '0'}}>Click the PDF link to download the Timetable</h5>
            </div>
            <div id="time-table-1">
              <Row>
                <Col xs="12" sm="8" lg="8">
                  <div id="time-table-1-left" class="card-head">
                    <h1>TIME TABLE</h1>
                    <h5>
                    <span ><a href={require("./1.pdf")} download={'Time-Table-6-7am+6-7pm.pdf'} style={{'color':'black'}}>CLICK TO DOWNLOAD</a></span></h5>
                    <AnchorLink offset='60' href='#sec1'> <h5>TIMETABLE: 6-7am + 6-7pm.pdf (Click here)</h5> </AnchorLink>
                  </div>
                </Col>
                <Col xs="12" sm="4" lg="4">
                  <div id="time-table-1-right">
                    <h1>TIME SLOT</h1>
                    <h2>(6-7am + 6-7pm)</h2>
                  </div>
                </Col>
              </Row>
            </div>

            <div id="time-table-2">
              <Row>
                <Col xs="12" sm="8" lg="8">
                  <div id="time-table-2-left" class="card-head">
                    <h1>TIME TABLE</h1>
                    <h5>
                    <span ><a href={require("./2.pdf")} style={{'color':'black'}} download={'Time-Table-6-8am+6-8pm.pdf'}>CLICK TO DOWNLOAD</a></span></h5>
                    <AnchorLink offset='60' href='#sec2'><h5>TIMETABLE: 6-8am + 6-8pm.pdf (Click here) </h5> </AnchorLink>
                  </div>
                </Col>
                <Col xs="12" sm="4" lg="4">
                  <div id="time-table-2-right">
                    <h1>TIME SLOT</h1>
                    <h2>(6-8am + 6-8pm)</h2>
                  </div>
                </Col>
              </Row>
            </div>

            <div id="time-table-3">
              <Row>
                <Col xs="12" sm="8" lg="8">
                  <div id="time-table-3-left" class="card-head">
                    <h1>TIME TABLE</h1>
                    <h5>
                    <span ><a href={require("./3.pdf")} download={'Time-Table-5-8pm.pdf'} style={{'color':'black'}}>CLICK TO DOWNLOAD</a></span></h5>
                    <AnchorLink offset='60' href='#sec3'><h5>TIMETABLE: 5-8pm.pdf (Click here)  </h5></AnchorLink>
                  </div>
                </Col>
                <Col xs="12" sm="4" lg="4">
                  <div id="time-table-3-right">
                    <h1>TIME SLOT</h1>
                    <h2>(5-8pm)</h2>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>

        <section id='sec1'>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Table id="table-1" bordered style={{'background-color':'WHITE'}}>
              <thead>
                <tr class="bordered">
                  <th id="tt-title1" class="card-head" colspan="5"><span>TIME TABLE OPTION (6-7am + 6-7pm)</span></th>
                </tr>
              </thead>

              <col class="bordered" width="10%"/>
              <col width="50%" class="bordered"/>
              <col width="7%" class="bordered"/>
              <col width="15%" class="bordered"/>
              <col width="10%" class="bordered"/>
              <thead class="card-head">
                <tr class="bordered">
                  <th class="middle" id="brown">MORNING SCHEDULE 6-7AM</th>
                  <th class="middle" id="brown">ACTIVITIES</th>
                  <th class="middle" id="brown">MINS</th>
                  <th id="brown"></th>
                  <th></th>
                </tr>
              </thead>
              <tbody class="cool-font">
                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">6-6.05AM</th>
                  <td class="cool">CHANTING OF HARE KRISHNA MAHA MANTRA IN CHORUS<br/>
                  PARENTS & CHILDREN SIT TOGETHER & RECITE 54 MAHA MANTRAS IN 5 MINS<br/>
                  MANTRAS ARE CHANTED IN 2 LOW PITCH AND 2 HIGH PITCH</td>
                  <td class="middle cool">5 MINS</td>
                  <td id="brown" class="middle"><b>JAPA</b></td>
                  <td rowspan="15" id="vert-align"><p class="text-center">6 AM - 7 AM</p></td>
                </tr>
                <tr class="bordered">
                  <td id="brown" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="brown"></td>
                </tr>

                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">6.05-6.10</th>
                  <td class="cool">MANTRA RECITATION<br/>
                  10 VERSES ARE SELECTED AND RECITED FOR 10 DAYS, EVERYDAY<br/>
                  METHOD OF RECITATION IS: <br/>
                  LEAD PERSON LEADS AND CHILDREN FOLLOW AND AFTER 4-5 DAYS OF RECITATION, CHILDREN LEAD AND PARENTS FOLLOW
                  <br/><br/>
                  WE TWICE RECITE THE VERSE AND ONCE READ THE MEANING</td>
                  <td class="middle cool">5 MINS</td>
                  <td id="brown" class="middle"><b>MANTRA RECITATION</b></td>
                </tr>
                <tr class="bordered">
                  <td id="brown" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="brown"></td>
                </tr>

                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">6.10-6.20</th>
                  <td class="cool">SHASTRA CLASS<br/>
                  ONE OF THE ABOVE 10 VERSES IS TAKEN AS A STORYBASED DISCUSSION SO THE CONCEPTS ARE CLEAR TO THE CHILDREN ABOUT WHAT THAT VERSE TALKS ABOUT
                  <br/><br/>
                  VERSES ARE SELECTED FROM:<br/>
                  SLOKA BOOK - FREQUENTLY RECITED VERSES BY SRILA PRABHUPADA<br/>
                  MUSUNDA MALA STOTRA<br/>
                  BRAHMA SAMHITA<br/>
                  FULL NOI<br/>
                  FULL ISOPANISAD
                  </td>
                  <td class="middle cool">10 MINS</td>
                  <td id="brown" class="middle"><b>SHASTRA STUDY</b></td>
                </tr>
                <tr class="bordered">
                  <td id="brown" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="brown"></td>
                </tr>

                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">6.20-6.30</th>
                  <td class="cool">CHILDREN GO FOR CLEANING AND ORGANISING THEIR PERSONAL BELONGINGS<br/><br/>
                    CHILDREN PLAN FOR THE DAY, WHAT ACTIVITIES THEY WOULD LIKE TO DO AND MENTALLY REHEARSE THEIR DAY IN THE MIND
                  <br/><br/>
                  </td>
                  <td class="middle cool">10 MINS</td>
                  <td id="brown" class="middle"><b>ORGANISING AND PLANNING</b></td>
                </tr>
                <tr class="bordered">
                  <td id="brown" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="brown"></td>
                </tr>

                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">6.30-6.50</th>
                  <td class="cool">YOGA & PRANAYAMA SESSION<br/>
                    (REFER BOOKLET)<br/><br/>
                    VARIETY IS MAINTAINED EVERY MONTH BY CHANGING A SET OF YOGASANAS, SO CHILDREN DONT GET BORED<br/><br/>
                    WHILE DOING YOGASANAS, WE PLAY VERY SOFT KIRTAN OR SOFT SOOTHING MUSIC IN THE BACKGROUND, SO THE MIND CAN BE RELAXED
                  </td>
                  <td class="middle cool">20 MINS</td>
                  <td id="brown" class="middle"><b>YOGA</b></td>
                </tr>
                <tr class="bordered">
                  <td id="brown" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="brown"></td>
                </tr>

                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">6.50-6.55</th>
                  <td class="cool">DISCUSSION ON SRILA PRABHUPADA KATHA - <br/>
                    RELEVANT STORIES<br/>
                    ANECDOTES FROM SRILA PRABHUPADA'S LIFE<br/>
                    WE READ OUT THE PASTIME AND DISCUSS ABOUT THE KEY LEARNINGS - <br/>
                    HOW A PURE DEVOTEE INTERACTS WITH OTHERS IN THE SOCIETY<br/>
                    HOW HE CONDUCTS HIS PERSONAL DAILY ROUTINE / SERVICES?<br/>
                    HOW HE ORGANISES AND MANAGES THE PREACHING MISSING OF ISKON?<br/><br/>
                    KATHA IS TAKEN FROM: <br/><br/>
                    SRILA PRABHUPADA NECTAR<br/>
                    WHAT IS THE DIFFICULTY<br/>
                    MY GLORIOUS MASTER<br/>
                    SRILA PRABHUPADA LILAMRTA<br/>
                    OTHER BIOGRAPHIES OF SRILA PRABHUPADA<br/><br/>

                  </td>
                  <td class="middle cool">5 MINS</td>
                  <td id="brown" class="middle"><b>SRILA PRABHUPADA NECTAR DROPS</b></td>
                </tr>
                <tr class="bordered">
                  <td id="brown" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="brown"></td>
                </tr>

                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">6.55-7.00</th>
                  <td class="cool">GURU PUJA OF SRILA PRABHUPADA<br/>
                    SHORT KIRTAN COMPLETED WITH JAYADVANI PRAYERS IN THE END
                  </td>
                  <td class="middle cool">5 MINS</td>
                  <td id="brown" class="middle"><b>GURU PUJA</b></td>
                </tr>
                <tr class="bordered">
                  <td id="brown" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="brown"></td>
                </tr>

                <tr class="bordered">
                  <th id="brown" class="middle" scope="row">7-7.05</th>
                  <td class="cool">HEALTHY NUTRIOUS PRASADAM<br/>
                    JAGGERY & KURMURA<br/>
                    SPROUTS & DATES
                  </td>
                  <td class="middle cool">5 MINS</td>
                  <td id="brown" class="middle"><b>PRASADA</b></td>
                </tr>
                <tr class="bordered">
                  <td colspan="5" class="big-row"></td>
                </tr>
              </tbody>
            </Table>

            <Table id="table-below" bordered style={{'background-color': 'WHITE'}}>
                <col width="10%" class="bordered"/>
                <col width="50%" class="bordered"/>
                <col width="7%" class="bordered"/>
                <col width="15%" class="bordered"/>
                <col width="10%" class="bordered"/>

                <thead class="card-head">
                  <tr id="brown">
                    <th class="middle">EVENING SCHEDULE 6-7.15PM</th>
                    <th class="middle">ACTIVITIES - KA KHE GA</th>
                    <th class="middle">MINS</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="cool-font">

                  <tr class="bordered">
                    <th id="brown" class="middle" scope="row">6-6.50</th>
                    <td class="cool"><b>KHEL</b><br/>
                    PLAY WITH THE KIDS - PANCHAKOSA VIKAS GAMES<br/>
                    EACH GAME HAS A PARTICULAR PURPOSE<br/>
                    -DEVELOPING FOCUS<br/>
                    -DEVELOPING STRENGTH<br/>
                    -DEVELOPING FLEXIBILITY<br/>
                    </td>
                    <td class="middle cool">50 MINS</td>
                    <td id="brown" class="middle"><b>PLAY TIME</b></td>
                    <td rowspan="5" id="vert-align"><p class="text-center">6 PM - 7 PM</p></td>
                  </tr>
                  <tr class="bordered">
                    <td id="brown" class="big-row"></td>
                    <td></td>
                    <td></td>
                    <td id="brown"></td>
                  </tr>

                  <tr class="bordered">
                    <th id="brown" class="middle" scope="row">6.50-7.05</th>
                    <td class="cool"><b>KAHANI / KATHA - SOLID FROM SCRIPTURES</b><br/><br/>
                    STUDY RAMAYANA IN HINDI<br/>
                    (GITAPRESS GORAKHPUR VERSION)<br/><br/>
                    OTHER BOOKS TO FOLLOW - <br/>
                    SB IN STORY FORM - Purnaprajna Dasa CONDENSED SB<br/>
                    CC CONDENSED FORM<br/>
                    MAHABHARATA<br/>
                    PANCHATANTRA<br/>
                    HITOPADESHA<br/><br/>
                    KEEP IT IN STORYBASE FORM. NOT TOO MUCH PHILOSOPHY, LESSONS. CHILDREN ARE VERY SMART. THEY PICK UP THE LESSONS SUBTLY AND APPROPRIATELY BY THEMSELVES.
                    </td>
                    <td class="middle cool">15 MINS</td>
                    <td id="brown" class="middle"><b>STORY TIME IN CONDENSED FORM</b></td>
                  </tr>
                  <tr class="bordered">
                    <td id="brown" class="big-row"></td>
                    <td></td>
                    <td></td>
                    <td id="brown"></td>
                  </tr>

                  <tr class="bordered">
                    <th id="brown" class="middle" scope="row">7.05-7.15</th>
                    <td class="cool"><b>GANA</b><br/><br/>
                    VAISNAVA SONGS WITH MEANING<br/>
                    OTHER SONGS WHICH ADD VALUE TO THE CHILD'S CONSCIOUSNESS, RATHER THAN JUST ENTERTAIN THEM<br/>
                    SONGS ARE STILL BEING COLLECTED. ONCE COMPLETE, WE CAN MAKE A SYLLABUS OUT OF IT<br/>
                    <br/>
                    SONGS THAT -<br/>
                    GIVE AESTHETIC APPRECIATION<br/>
                    HELP US SEE NATURE IN RESPECTFUL SPIRIT<br/>
                    DEVELOP APPRECIATION FOR BEING TRUTHFUL<br/>
                    DEVELOP APPRECIATION FOR BEING SPIRITUAL ACTIVIST
                    </td>
                    <td class="middle cool">10 MINS</td>
                    <td id="brown" class="middle"><b>SINGING TIME</b></td>
                  </tr>

                </tbody>
            </Table>
          </Col>
        </Row>
        </section>
		<section id='sec2'>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Table id="table-2" bordered style={{'background-color':'WHITE'}}>
              <thead>
                <tr class="bordered">
                  <th id="tt-title2" class="card-head" colspan="5"><span>TIME TABLE OPTION (6-8am + 6-8pm)</span></th>
                </tr>
              </thead>

              <col width="10%"/>
              <col width="50%"/>
              <col width="7%"/>
              <col width="15%"/>
              <col width="10%"/>
              <thead class="card-head">
                <tr class="bordered">
                  <th class="middle" id="green">MORNING SCHEDULE 6-8AM</th>
                  <th class="middle" id="green">ACTIVITIES</th>
                  <th class="middle" id="green">MINS</th>
                  <th id="green"></th>
                </tr>
              </thead>
              <tbody class="cool-font">
                <tr class="bordered">
                  <th id="green" class="middle" scope="row">6-6.15AM</th>
                  <td class="cool">CHANTING OF HARE KRISHNA MAHA MANTRA IN CHORUS<br/>
                  PARENTS & CHILDREN SIT TOGETHER & RECITE 54 MAHA MANTRAS IN 5 MINS<br/>
                  MANTRAS ARE CHANTED IN 2 LOW PITCH AND 2 HIGH PITCH</td>
                  <td class="middle cool">15 MINS</td>
                  <td id="green" class="middle"><b>JAPA</b></td>
                  <td rowspan="15" id="vert-align"><p class="text-center">6 AM - 8 AM</p></td>
                </tr>
                <tr class="bordered">
                  <td id="green" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="green"></td>
                </tr>

                <tr class="bordered">
                  <th id="green" class="middle" scope="row">6.15-6.30</th>
                  <td class="cool">ASSENBLY -LITTLE DROPS TO INTRODUCE SRILA PRABHUPADA<br/><br/>
					DISCUSSION ON SRILA PRABHUPADA KATHA - <br/>
                    RELEVANT STORIES<br/>
                    ANECDOTES FROM SRILA PRABHUPADA'S LIFE<br/>
                    WE READ OUT THE PASTIME AND DISCUSS ABOUT THE KEY LEARNINGS - <br/>
                    HOW A PURE DEVOTEE INTERACTS WITH OTHERS IN THE SOCIETY<br/>
                    HOW HE CONDUCTS HIS PERSONAL DAILY ROUTINE / SERVICES?<br/>
                    HOW HE ORGANISES AND MANAGES THE PREACHING MISSING OF ISKON?<br/><br/>
                    KATHA IS TAKEN FROM: <br/><br/>
                    SRILA PRABHUPADA NECTAR<br/>
                    WHAT IS THE DIFFICULTY<br/>
                    MY GLORIOUS MASTER<br/>
                    SRILA PRABHUPADA LILAMRTA<br/>
                    OTHER BIOGRAPHIES OF SRILA PRABHUPADA<br/><br/>	</td>
                  <td class="middle cool">15 MINS</td>
                  <td id="green" class="middle"><b>SRILA PRABHUPADA NECTAR DROPS</b></td>
                </tr>
                <tr class="bordered">
                  <td id="green" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="green"></td>
                </tr>

                <tr class="bordered">
                  <th id="green" class="middle" scope="row">6.30-6.40</th>
                  <td class="cool">MANTRA RECITATION<br/>
                  10 VERSES ARE SELECTED AND RECITED FOR 10 DAYS, EVERYDAY<br/>
                  METHOD OF RECITATION IS: <br/>
                  LEAD PERSON LEADS AND CHILDREN FOLLOW AND AFTER 4-5 DAYS OF RECITATION, CHILDREN LEAD AND PARENTS FOLLOW
                  <br/><br/>
                  WE TWICE RECITE THE VERSE AND ONCE READ THE MEANING</td>
                  <td class="middle cool">10 MINS</td>
                  <td id="green" class="middle"><b>MANTRA RECITATION</b></td>
                </tr>
                <tr class="bordered">
                  <td id="green" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="green"></td>
                </tr>

                <tr class="bordered">
                  <th id="green" class="middle" scope="row">6.40-6.50</th>
                  <td class="cool">SHASTRA CLASS<br/>
                  ONE OF THE ABOVE 10 VERSES IS TAKEN AS A STORYBASED DISCUSSION SO THE CONCEPTS ARE CLEAR TO THE CHILDREN ABOUT WHAT THAT VERSE TALKS ABOUT
                  <br/><br/>
                  VERSES ARE SELECTED FROM:<br/>
                  SLOKA BOOK - FREQUENTLY RECITED VERSES BY SRILA PRABHUPADA<br/>
                  MUSUNDA MALA STOTRA<br/>
                  BRAHMA SAMHITA<br/>
                  FULL NOI<br/>
                  FULL ISOPANISAD
                  </td>
                  <td class="middle cool">10 MINS</td>
                  <td id="green" class="middle"><b>SHASTRA CLASS</b></td>
                </tr>
                <tr class="bordered">
                  <td id="green" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="green"></td>
                </tr>

                <tr class="bordered">
                  <th id="green" class="middle" scope="row">6.50-7.15</th>
                  <td class="cool">YOGA & PRANAYAMA SESSION<br/>
                    (REFER BOOKLET)<br/><br/>
                    VARIETY IS MAINTAINED EVERY MONTH BY CHANGING A SET OF YOGASANAS, SO CHILDREN DONT GET BORED<br/><br/>
                    WHILE DOING YOGASANAS, WE PLAY VERY SOFT KIRTAN OR SOFT SOOTHING MUSIC IN THE BACKGROUND, SO THE MIND CAN BE RELAXED
                  </td>
                  <td class="middle cool">25 MINS</td>
                  <td id="green" class="middle"><b>YOGA</b></td>
                </tr>
                <tr class="bordered">
                  <td id="green" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="green"></td>
                </tr>

                <tr class="bordered">
                  <th id="green" class="middle" scope="row">7.15-7.35</th>
                  <td class="cool">CHILDREN GO FOR CLEANING AND ORGANISING THEIR PERSONAL BELONGINGS<br/><br/>
                    CHILDREN PLAN FOR THE DAY, WHAT ACTIVITIES THEY WOULD LIKE TO DO AND MENTALLY REHEARSE THEIR DAY IN THE MIND
                  <br/><br/>
                  </td>
                  <td class="middle cool">20 MINS</td>
                  <td id="green" class="middle"><b>ORGANISING & PLANNING</b></td>
                </tr>
                <tr >
                  <td id="green" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="green"></td>
                </tr>

                <tr class="bordered">
                  <th id="green" class="middle" scope="row">7.35-7.45</th>
                  <td class="cool">GURU PUJA OF SRILA PRABHUPADA<br/>
                    SHORT KIRTAN COMPLETED WITH JAYADVANI PRAYERS IN THE END
                  </td>
                  <td class="middle cool">10 MINS</td>
                  <td id="green" class="middle"><b>GURU PUJA</b></td>
                </tr>
                <tr class="bordered">
                  <td id="green" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="green"></td>
                </tr>

                <tr class="bordered">
                  <th id="green" class="middle" scope="row">7.45-8</th>
                  <td class="cool">HEALTHY NUTRIOUS PRASADAM<br/>
                    JAGGERY & KURMURA<br/>
                    SPROUTS & DATES
                  </td>
                  <td class="middle cool">15 MINS</td>
                  <td id="green" class="middle"><b>PRASADA</b></td>
                </tr>
                <tr class="bordered">
                  <td colspan="5" class="big-row"></td>
                </tr>
              </tbody>
            </Table>
            <Table id="table-below" bordered style={{'background-color': 'WHITE'}}>
                <col width="10%" class="bordered"/>
                <col width="50%" class="bordered"/>
                <col width="7%" class="bordered"/>
                <col width="15%" class="bordered"/>
                <col width="10%" class="bordered"/>

                <thead class="card-head">
                  <tr id="green">
                    <th class="middle">EVENING SCHEDULE 6-8PM</th>
                    <th class="middle">ACTIVITIES - KA KHE GA</th>
                    <th class="middle">MINS</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="cool-font">

                  <tr class="bordered">
                    <th id="green" class="middle" scope="row">6-6.10PM</th>
                    <td class="cool"><b>PANCHAKOSA VIKAS</b><br/>
                    WALK<br/>
					TREK<br/>
					JOG<br/>
                    </td>
                    <td class="middle cool">10 MINS</td>
                    <td id="green" class="middle"><b>PLAY TIME</b></td>
                    <td rowspan="11" id="vert-align"><p class="text-center">6 PM - 8 PM</p></td>
                  </tr>
                  <tr class="bordered">
                    <td id="green" class="big-row"></td>
                    <td></td>
                    <td></td>
                    <td id="green"></td>
                  </tr>

                  <tr class="bordered">
                    <th id="green" class="middle" scope="row">6.10-7</th>
                    <td class="cool"><b>COOKING/KHEL -PLAY -COORDINATED INDOOR & OUTDOOR GAMES</b><br/>
                    PLAY WITH THE KIDS - PANCHAKOSA VIKAS GAMES<br/>
                    EACH GAME HAS A PARTICULAR PURPOSE<br/>
                    -DEVELOPING FOCUS<br/>
                    -DEVELOPING STRENGTH<br/>
                    -DEVELOPING FLEXIBILITY<br/>
                    </td>
                    <td class="middle cool">50 MINS</td>
                    <td id="green" class="middle"><b>PLAY TIME</b></td>
                  </tr>
                  <tr class="bordered">
                    <td id="green" class="big-row"></td>
                    <td></td>
                    <td></td>
                    <td id="green"></td>
                  </tr>

                  <tr class="bordered">
                    <th id="green" class="middle" scope="row">7-7.20</th>
                    <td class="cool"><b>KAHANI / KATHA - SOLID FROM SCRIPTURES</b><br/><br/>
                    STUDY RAMAYANA IN HINDI<br/>
                    (GITAPRESS GORAKHPUR VERSION)<br/><br/>
                    OTHER BOOKS TO FOLLOW - <br/>
                    SB IN STORY FORM - Purnaprajna Dasa CONDENSED SB<br/>
                    CC CONDENSED FORM<br/>
                    MAHABHARATA<br/>
                    PANCHATANTRA<br/>
                    HITOPADESHA<br/><br/>
                    KEEP IT IN STORYBASE FORM. NOT TOO MUCH PHILOSOPHY, LESSONS. CHILDREN ARE VERY SMART. THEY PICK UP THE LESSONS SUBTLY AND APPROPRIATELY BY THEMSELVES.
                    </td>
                    <td class="middle cool">15 MINS</td>
                    <td id="green" class="middle"><b>STORY TIME IN CONDENSED FORM</b></td>
                  </tr>
				  <tr class="bordered">
                    <td id="green" class="big-row"></td>
                    <td></td>
                    <td></td>
                    <td id="green"></td>
                  </tr>
				  <tr class="bordered">
                    <th id="green" class="middle" scope="row">7.20-7.30</th>
                    <td class="cool"><b>GANA</b><br/><br/>
                    VAISNAVA SONGS WITH MEANING<br/>
                    OTHER SONGS WHICH ADD VALUE TO THE CHILD'S CONSCIOUSNESS, RATHER THAN JUST ENTERTAIN THEM<br/>
                    SONGS ARE STILL BEING COLLECTED. ONCE COMPLETE, WE CAN MAKE A SYLLABUS OUT OF IT<br/>
                    <br/>
                    SONGS THAT -<br/>
                    GIVE AESTHETIC APPRECIATION<br/>
                    HELP US SEE NATURE IN RESPECTFUL SPIRIT<br/>
                    DEVELOP APPRECIATION FOR BEING TRUTHFUL<br/>
                    DEVELOP APPRECIATION FOR BEING SPIRITUAL ACTIVIST
                    </td>
					          <td class="middle cool">10 MINS</td>
                    <td id="green" class="middle"><b>SINGING TIME</b></td>
                  </tr>
				          <tr class="bordered">
                    <td id="green" class="big-row"></td>
                    <td></td>
                    <td></td>
                    <td id="green"></td>
                  </tr>
				          <tr class="bordered">
                    <th id="green" class="middle" scope="row">7.30-</th>
                    <td class="cool"><b>HOT MILK</b><br/><br/>
                   </td>
					          <td class="middle"></td>
                    <td id="green" class="middle"><b>MILK TIME</b></td>
                  </tr>
				  <tr class="bordered">
                    <td id="green" class="big-row"></td>
                    <td></td>
                    <td></td>
                    <td id="green"></td>
                  </tr>
				  <tr class="bordered">
                    <th id="green" class="middle" scope="row">8.00-</th>
                    <td class="cool"><b>REST</b><br/><br/>
                   </td>
					<td class="middle"></td>
                    <td id="green" class="middle"><b>REST</b></td>
                  </tr>


                </tbody>
            </Table>
          </Col>
        </Row>
        </section>

		<section id='sec3'>
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Table id="table-1" bordered style={{'background-color':'WHITE'}}>
              <thead>
                <tr class="bordered">
                  <th id="tt-title3" class="card-head" colspan="5"><span>TIME TABLE OPTION (5pm - 8pm)</span></th>
                </tr>
              </thead>

              <col width="10%"/>
              <col width="50%"/>
              <col width="7%"/>
              <col width="15%"/>
              <col width="10%"/>
              <thead class="card-head">
                <tr class="bordered">
                  <th class="middle" id="violet">EVENING SCHEDULE 5-8PM</th>
                  <th class="middle" id="violet">ACTIVITIES</th>
                  <th class="middle" id="violet">MINS</th>
                  <th id="violet"></th>
                </tr>
              </thead>
              <tbody class="cool-font">
                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">5-5.20</th>
                  <td class="cool">YOGA AND PRANAYAM SESSION<br/>
                  (REFER BOOKLET)<br/><br/>
                  VARIETY IS MAINTAINED EVERY MONTH BY CHANGING A SET OF YOGASANAS, SO CHILDREN DON'T GET BORED</td>
                  <td class="middle cool">20 MINS</td>
                  <td id="violet" class="middle"><b>YOGA</b></td>
                  <td rowspan="17" id="vert-align"><p class="text-center">5 PM - 8 PM</p></td>
                </tr>
                <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">5.20-5.25</th>
                  <td class="cool">CHANTING OF HARE KRISHNA MAHA MANTRA IN CHORUS<br/>
                  PARENTS & CHILDREN SIT TOGETHER & RECITE 54 MAHA MANTRAS IN 5 MINS<br/>
                  MANTRAS ARE CHANTED IN 2 LOW PITCH AND 2 HIGH PITCH</td>
                  <td class="middle cool">5 MINS</td>
                  <td id="violet" class="middle"><b>JAPA</b></td>
                </tr>
                <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">5.25-5.40</th>
                  <td class="cool">ASSEMBLY ‐ LITTLE DROPS TO INTRODUCE SRILA PRABHUPADA<br/><br/>
				  DISCUSSION ON SRILA PRABHUPADA KATHA -<br/>
				  RELEVANT STORIES <br/>
				  ANECDOTES FROM SRILA PRABHUPADA'S LIFE<br/>
				  WE READ OUT THE PASTIME AND DISCUSS ABOUT THE KEY LEARNINGS -<br/>
				  HOW A PURE DEVOTEE INTERACTS WITH OTHERS IN THE SOCIETY?<br/>
				  HOW HE CONDUCTS HIS PERSONAL DAILY ROUTINE / SERVICES?<br/>
				  HOW HE ORGANISES AND MANAGES THE PREACHING MISSION OF ISKCON?<br/><br/>
				  KATHA IS TAKEN FROM:<br/><br/>
				  SRILA PRABHUPADA NECTAR<br/>
				  WHAT IS THE DIFFICULTY?<br/>
				  MY GLORIOUS MASTER<br/>
				  SRILA PRABHUPADA LILAMRTA<br/>
				  OTHER BIOGRAPHIES OF SRILA PRABHUPADA
                  </td>
                  <td class="middle cool">15 MINS</td>
                  <td id="violet" class="middle"><b>SRILA PRABHUPADA<br/>NECTAR DROPS</b></td>
                </tr>
                <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">5.40-6.30</th>
                  <td class="cool">
				  <b>COOKING / KHEL - PLAY - COORDINATED INDOOR & OUTDOOR GAMES</b><br/><br/>
				  PLAY WITH THE KIDS - PANCHAKOSA VIKAS GAMES<br/>
				  EACH GAME HAS A PARTICULAR PURPOSE<br/>
				  -DEVELOPING FOCUS<br/>
				  -DEVELOPING TEAM SPIRIT<br/>
				  -DEVELOPING STRENGTH<br/>
				  -DEVELOPING FLEXIBILITY
                  </td>
                  <td class="middle cool">50 MINS</td>
                  <td id="violet" class="middle"><b>PLAY TIME</b></td>
                </tr>
                <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">6.30-6.35</th>
                  <td class="cool">
					MANTRA RECITATION<br/>
					10 VERSES ARE SELECTED AND RECITED FOR 10 DAYS, EVERYDAY<br/>
					METHOD OF RECITATION IS:<br/>
					LEAD PERSON LEADS AND CHILDREN FOLLOW AND AFTER 4 - 5 DAYS OF RECITATION,<br/>
					CHILDREN LEAD AND PARENTS FOLLOW<br/><br/>
					WE TWICE RECITE THE VERSE AND ONCE READ THE MEANING
                  </td>
                  <td class="middle cool">5 MINS</td>
                  <td id="violet" class="middle"><b>MANTRA RECITATION</b></td>
                </tr>
                <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">6.35-6.50</th>
                  <td class="cool">
					SHASTRA CLASS<br/>
					ONE OF THE ABOVE 10 VERSES IS TAKEN AS A STORYBASED DISCUSSION SO THE CONCEPTS<br/>
					ARE CLEAR TO THE CHILDREN ABOUT WHAT THAT VERSE TALKS ABOUT<br/><br/>
					VERSES ARE SELECTED FROM:<br/>
					SLOKA BOOK - FREQUENTLY RECITED VERSES BY SRILA PRABHUPADA<br/>
					MUKUNDA MALA STOTRA<br/>
					BRAHMA SAMHITA<br/>
					FULL NOI<br/>
					FULL ISOPANISAD
                  </td>
                  <td class="middle cool">15 MINS</td>
                  <td id="violet" class="middle"><b>SHASTRA STUDY</b></td>
                </tr>
                <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">6.50-7.20</th>
                  <td class="cool">
					<b>KAHANI / KATHA - SOLID FROM SCRIPTURES (DRAMATIC RENDITIONS)</b><br/><br/>
					STUDY RAMAYANA IN HINDI<br/>
					(GITAPRESS GORAKHPUR VERSION)<br/><br/>
					OTHER BOOKS TO FOLLOW - <br/>
					SB IN STORY FORM -Purnaprajna Dasa CONDENSED SB<br/>
					CC CONDENSED FORM<br/>
					MAHABHARATA<br/>
					PANCHATANTRA<br/>
					HITOPADESHA<br/><br/>
					KEEP IT IN STORYBASE FORM. NOT TOO MUCH PHILOSOPHY, LESSONS. CHILDREN ARE<br/>
					VERY SMART. THEY PICK UP THE LESSONS SUBTLY AND APPROPRIATELY BY THEMSELVES.
				  </td>
                  <td class="middle cool">30 MINS</td>
                  <td id="violet" class="middle"><b>STORY TIME<br/>CONDENSED FORM</b></td>
                </tr>
                <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

                <tr class="bordered">
                  <th id="violet" class="middle" scope="row">7.20-7.30</th>
                  <td class="cool"><b>GANA</b><br/>
                    VAISNAVA SONGS WITH MEANING<br/>
					OTHER SONGS WHICH ADD VALUE TO THE CHILD'S CONSCIOUSNESS, RATHER THAN JUST<br/>
					ENTERTAIN THEM.<br/>
					SONGS ARE STILL BEING COLLECTED. ONCE COMPLETE, WE CAN MAKE A SYLLABUS OUT OF<br/>
					IT.<br/><br/>
					SONGS THAT -<br/>
					GIVE AESTHETIC APPRECIATION<br/>
					HELP US SEE NATURE IN RESPECTFUL SPIRIT<br/>
					DEVELOP APPRECIATION FOR BEING TRUTHFUL<br/>
					DEVELOP APPRECIATION FOR BEING SPIRITUAL ACTIVIST

                  </td>
                  <td class="middle cool">10 MINS</td>
                  <td id="violet" class="middle"><b>SINGING TIME</b></td>
                </tr>

                 <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

				<tr class="bordered">
                  <th id="violet" class="middle" scope="row">7.20-7.30</th>
                  <td class="cool">
					ORGANISING THEIR PERSONAL BELONGINGS<br/><br/>
					CHILDREN PLAN FOR THE NEXT DAY, WHAT ACTIVITIES THEY WOULD LIKE TO DO AND<br/>
					MENTALLY REHEARSE THEIR DAY IN THE MIND ‐ GOAL ORIENTED ACTIVITIES

                  </td>
                  <td class="middle cool">15 MINS</td>
                  <td id="violet" class="middle"><b>ORGANISING &<br/>PLANNING</b></td>
                </tr>

				 <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>

				<tr class="bordered">
                  <th id="violet" class="middle" scope="row">7.45-</th>
                  <td class="cool">HOT MILK
                  </td>
                  <td class="middle"></td>
                  <td id="violet" class="middle"><b>MILK TIME</b></td>
                </tr>

				 <tr class="bordered">
                  <td id="violet" class="big-row"></td>
                  <td></td>
                  <td></td>
                  <td id="violet"></td>
                </tr>


				<tr class="bordered">
                  <th id="violet" class="middle" scope="row">8.00-</th>
                  <td class="cool">
				  REST
                  </td>
                  <td class="middle"></td>
                  <td id="violet" class="middle"><b>REST TIME</b></td>
                </tr>
              </tbody>
            </Table>


          </Col>
        </Row>
        </section>
      </div>





    );
  }
}

export default Timetable;
